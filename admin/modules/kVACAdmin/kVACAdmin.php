<?php

/*
 * kVAC Admin Settings
 * By: Jeffrey Kobus
 * www.fs-products.net 
 * 02/21/2018
 * v1.0.8.0
 */

class kVACAdmin extends CodonModule
{

    public function HTMLHead()
    {
        $this->set('sidebar', 'kvac/kvac_settings_sidebar.php');
    }

    public function NavBar()
    {
        if (PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            echo '<li><a href="' . SITE_URL . '/admin/index.php/kVACAdmin">kVAC Settings</a></li>';
        }
    }

    public function index()
    {
        $this->settings();
    }

    public function settings()
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'addsetting':
                    $this->AddSetting();
                    break;
                case 'savesettings':
                    $this->saveSettingsPost();
                    break;
            }
        }
        $this->showSettings();
    }

    protected function showSettings()
    {
        $settings = kVACData::getSettings();
        $this->set('settings', $settings);
        $this->render('kvac/kvac_settings_main.php');
    }

    protected function saveSettingsPost()
    {
        while (list($name, $value) = each($_POST))
        {

            if ($name == 'action')
                continue;
            elseif ($name == 'submit')
                continue;

            if ($value == 'on')
                $value = 1;
            if ($value == 'off')
                $value = 0;

            $value = DB::escape($value);

            kVACData::SaveSetting($name, $value);
        }
        $this->set('message', 'kVAC Settings were saved!');
        $this->render('core_success.php');
    }

    public function resetToDefault()
    {
        kVACData::resetTable_Settings();

        $this->index();
        $this->set('message', 'kVAC Settings were reset!');
        $this->render('core_success.php');
    }

    public function update()
    {
        kVACData::installRows_Settings(false);

        $this->index();
        $this->set('message', 'kVAC Settings were updated!');
        $this->render('core_success.php');
    }
    
}

