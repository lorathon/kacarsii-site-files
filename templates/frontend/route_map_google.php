<h3>Route Map</h3>
<div class="mapcenter" align="center">
    <div id="routemap" style="width: <?php echo Config::Get('MAP_WIDTH'); ?>; height: <?php echo Config::Get('MAP_HEIGHT') ?>"></div>
</div>
<?php
/**
 * 
 * This is the new Google Maps v3 code. Be careful of changing
 * things here, only do something if you know what you're doing.
 * 	          
 * These are some options for the map, you can change here.
 * 
 * This map is used for schedules and PIREPS
 * 
 * By default, the zoom level and center are ignored, and the map 
 * will try to fit the all the flights in. If you want to manually set
 * the zoom level and center, set "autozoom" to false.
 * 
 * If you want to adjust the size of the map - Look at the above
 * "routemap" div with the CSS width/height parameters. You can 
 * easily adjust it from there.
 * 
 * And for reference, you want to tinker:
 * http://code.google.com/apis/maps/documentation/v3/basics.html
 */
if (isset($pirep))
    $mapdata = $pirep;
if (isset($schedule))
    $mapdata = $schedule;


$image = fileurl('/images/airline/' . $pirep->code . '.gif');
?>
<?php
/* 	This is a small template for information about a navpoint popup 

  Variables available:

  <%=nav.title%>
  <%=nav.name%>
  <%=nav.freq%>
  <%=nav.lat%>
  <%=nav.lng%>
  <%=nav.type%>	2=NDB 3=VOR 4=DME 5=FIX 6=TRACK
 */
?>




<script type="text/html" id="navpoint_bubble">
    <span style="font-size: 10px; text-align:left; width: 100%" align="left">
        <strong>Name: </strong><%=nav.title%> (<%=nav.name%>)<br />
        <strong>Type: </strong>
<?php /* Show the type of point */ ?>
        <% if(nav.type == 2) { %> NDB <% } %>
        <% if(nav.type == 3) { %> VOR <% } %>
        <% if(nav.type == 4) { %> DME <% } %>
        <% if(nav.type == 5) { %> FIX <% } %>
        <% if(nav.type == 6) { %> TRACK <% } %>
        <br />
        <?php /* Only show frequency if it's not a 0 */ ?>
        <% if(nav.freq != 0) { %>
        <strong>Frequency: </strong><%=nav.freq%>
        <% } %>
    </span>
</script>

        <?php
        /*
          IMAGES IN THE POSITION INFO BUBBLE
          In the Bubble template below you can add img refernces
          Links, etc.  use standard HTML coding
          <img src="http://www.airlinelogos.net/albums/p/panam-old.gif"><br />
          The above link can be added to the space below.  at the top of the
          bubble will be displayed the Pan Am logo.  Obviously you can
          change this to what every you wish.

          FYI - The same applies to the acarsmap.tpl
         */
        ?>	

<script type="text/html" id="position_bubble">
    <span style="font-size: 10px; text-align:left; width: 100%" align="left">

        <strong>Altitude: </strong><%=nav.alt%><br />
        <strong>Ground Speed: </strong><%=nav.gs%><br />
        <strong>Heading: </strong><%=nav.head%><br />
        <strong>Phase: </strong><%=nav.phase%><br />
        <strong>Latitude: </strong><%=nav.lat%><br />
        <strong>Longitude: </strong><%=nav.lng%><br />
<?php /* Only show frequency if it's not a 0 */ ?>
        <% if(nav.warn != 0) { %>
        <strong>Warning: </strong><%=nav.warndetail%>
        <% } %>

    </span>
</script>


<?php
/* 	Below here is all the javascript for the map. Be careful of what you
  modify!! */

$icondep = fileurl('/lib/images/icon_dep.png');
$iconarr = fileurl('/lib/images/icon_arr.png');
?>
<script type="text/javascript">
    var options = {
    mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var map = new google.maps.Map(document.getElementById("routemap"), options);
    var dep_location = new google.maps.LatLng(<?php echo $mapdata->deplat ?>,<?php echo $mapdata->deplng; ?>);
    var arr_location = new google.maps.LatLng(<?php echo $mapdata->arrlat ?>,<?php echo $mapdata->arrlng; ?>);
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(dep_location);
    bounds.extend(arr_location);
    var depMarker = new google.maps.Marker({
    position: dep_location,
            map: map,
            icon: "<?php echo $icondep; ?>",
            title: "<?php echo $mapdata->depname; ?>"
            });
<?php
/* Populate the route */
if (is_array($mapdata->route_details))
{
    $list = array();
    foreach ($mapdata->route_details as $route)
    {
        if ($route->type == NAV_VOR)
        $icon = fileurl('/lib/images/icon_vor.png');
        else
        $icon = fileurl('/lib/images/icon_fix.png');
        $lat = str_replace(",", ".", $route -> lat);
        $lng = str_replace(",", ".", $route -> lng);
        /* 	Build info array for the bubble */
        ?>
        var v<?php echo $route->name; ?>_info =
        {
            freq: "<?php echo $route->freq ?>",
            name: "<?php echo $route->name ?>",
            title: "<?php echo $route->title ?>",
            type: "<?php echo $route->type ?>",
            lat: "<?php echo $lat ?>",
            lng: "<?php echo $lng ?>"
        };

        var v<?php echo $route->name; ?>_navpoint_info = tmpl("navpoint_bubble", {nav: v<?php echo $route->name ?>_info});
            var v<?php echo $route->name ?>_coords = new google.maps.LatLng(<?php echo $lat ?>, <?php echo $lng ?>);
            var v<?php echo $route->name ?>_marker = new google.maps.Marker({
            position: v<?php echo $route-> name; ?>_coords,
                    map: map,
                    icon: "<?php echo $icon; ?>",
                    title: "<?php echo $route->title; ?>",
                    infowindow_content: v<?php echo $route-> name; ?>_navpoint_info
                    });
            bounds.extend(v<?php echo $route->name ?>_coords);
            google.maps.event.addListener(v<?php echo $route->name ?>_marker, 'click', function()
                    {
                    info_window = new google.maps.InfoWindow({
                    content: this.infowindow_content,
                            position: this.position
                    });
                    info_window.open(map, this);
                    });
        <?php
        // For the polyline
        $list[] = "v{$route->name}_coords";
    }
}
?>
    var arrMarker = new google.maps.Marker({
    position: arr_location,
            map: map,
            icon: "<?php echo $iconarr; ?>",
            title: "<?php echo $mapdata->arrname; ?>"
    });
    var flightPath = new google.maps.Polyline({
    path: [dep_location, <?php if (count($list) > 0)
{
    echo implode(',', $list) . ',';
} ?> arr_location],
            strokeColor: "#FF0000", strokeOpacity: 1.0, strokeWeight: 2
            }).setMap(map);
    // Resize the view to fit it all in
    map.fitBounds(bounds);</script>


<script type="text/javascript">
<?php
/* Populate the route */

$res = @unserialize($mapdata->rawdata);
if ($res !== false)
{
    $mapdata -> rawdata = $res;
    unset($res);
}

if (is_array($mapdata->rawdata['points']))
{
    $list = array();
    foreach($mapdata -> rawdata['points'] as $point)
    {
        $detail = "";
        if ($point['alt'] < 10 && $point['warning'] == 0)
        {
            $icon = fileurl('/lib/images/onground.png');
        }
        if ($point['warning'] == 1)
        {
            $icon = fileurl('/lib/images/warning.png');
        }
        if ($point['alt'] > 10 && $point['warning'] == 0)
        {
            $icon = fileurl('lib/images/inair/' . $point['head'] . '.png');
        }

        if ($point['warning'] == 1)
        {
            $detail = ' | Warning: ' . $point['warningdetail'];
        }

        $latP = str_replace(",", ".", $point['lat']);
        $lngP = str_replace(",", ".", $point['lng']);

        if ($latP == '0')
            continue;


// FILL THIS IN WITH THE PROPER STUFF, ALL INSTANCES OF
// $route WITH $point
        ?>
            var v<?php echo $point['name']; ?>_info = {
            alt: "<?php echo $point['alt']; ?>",
                    gs: "<?php echo $point['gs']; ?>",
                    head: "<?php echo $point['head']; ?>",
                    phase: "<?php echo $point['phase']; ?>",
                    lat: "<?php echo round($latP, 4); ?>",
                    lng: "<?php echo round($lngP, 4); ?>",
                    warn: "<?php echo $point['warning']; ?>",
                    warndetail: "<?php echo $point['warningdetail']; ?>"
                    };
        // ALSO THE POINTS HERE
            var v<?php echo $point['name']; ?>_coords = new google.maps.LatLng(<?php echo $latP; ?>, <?php echo $lngP; ?>);
            var v<?php echo $point['name']; ?>_navpoint_info = tmpl("position_bubble", {nav: v<?php echo $point['name']; ?>_info});
        // YOU CAN ADD A CUSTOM ICON FROM HERE ON EACH POINT
            var v<?php echo $point['name']; ?>_marker = new google.maps.Marker({
            position: v<?php echo $point['name']; ?>_coords,
                    map: map,
                    icon: "<?php echo $icon; ?>", // THIS IS THE LINK TO AN ICON
                    title: "<?php echo $point['phase']; ?>",
                    infowindow_content: v<?php echo $point['name']; ?>_navpoint_info
            });
            bounds.extend(v<?php echo $point['name']; ?>_coords);
            google.maps.event.addListener(v<?php echo $point['name']; ?>_marker, 'click', function()
            {
            info_window = new google.maps.InfoWindow({
            content: this.infowindow_content,
                    position: this.position
                    });
            info_window.open(map, this);
            });
        <?php
// For the polyline
//$list = array();
        $list[] = "v{$point['name']}_coords";
    }
}
?>

    var kACARSPath = new google.maps.Polyline({
    path: [dep_location, <?php if (count($list) > 0)
{
    echo implode(',', $list) . ',';
} ?> arr_location],
            strokeColor: "#0000FF", strokeOpacity: 1.0, strokeWeight: 2
    }).setMap(map);
    // Resize the view to fit it all in
    map.fitBounds(bounds);
</script>