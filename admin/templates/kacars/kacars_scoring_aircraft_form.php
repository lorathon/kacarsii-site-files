<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<h3><?php echo $title;?></h3>

<p>These are settings will be used for the specific aircraft based on the ICAO.  If the ICAO code of the aircraft being 
    used matches one of these profiles and the scoring system has been activated then this profile will be used.  
    Any setting of 0 will be ignored.  All settings and points values must be integers.  Point values will reduce the current 
    score if the setting is exceeded.  All penalties will be logged to the flight log.  Some of these will override the standard
    scoring settings.  If the setting can be activated this means there is a standard setting that will be overridden.
</p>
<hr>
<ul>
    <li><strong>Setting</strong> - This is the threshold for the parameter.  If the Actual value is greater than or less than 
        (depending on parameter) the penalty will be triggered.  If this value is set to ZERO the parameter set will be ignored  
        This allows the application to run even if the DB table gets corrupted.
    </li>
    <li><strong>Value</strong> - This is the penalty point reduction/addition.  If the parameter is triggered this value is added to the 
        current score.  So if the value is negative it will reduce the current score by this amount.  If positive it will increase the 
        current score by this amount.
    </li>
    <li><strong>Delay</strong> - This is the number of seconds that the parameters setting must be triggered for before accessing the penalty. 
        If set to zero the parameter will be triggered as soon as the parameter is exceeded.
    </li>
    <li><strong>Repeat</strong> - If this is activated the parameter can be triggered again. This works in concert with the `Reset Value`. 
        If not activated then the parameter can only be triggered once per flight
    </li>
    <li><strong>Reset Value</strong> - This is used in concert with `Repeat`. If the `Repeat` is activate this value will be used to reset 
        the parameter so that in can be triggered again. If the `Repeat` is not active this value is not used. (TRUE/FALSE checks are always 
        reset on TRUE/FALSE transition if `Repeat` is active.)
    </li>    
    <li><strong>Active</strong> - This determines if this parameters set is used at all. If active then the parameter set will be monitored. 
        If not active the parameter set will be ignored.
    </li>
    <li><strong>Profile Active</strong> - This determines if this profile is used at all.  If active then the profile set will be monitored.  
        If not active the profile set will be ignored.
    </li>
</ul>
<hr>
<form action="<?php echo adminurl('/kacarsadmin/scoring_aircraft');?>" method="post">    
    <table id="tabledlist" class="tablesorter">
        <thead>
            <tr>
                <th>Name</th>
                <th>Setting</th>
                <th>Value</th>
                <th>Delay (seconds)</th>
                <th>Repeat</th>
                <th>Reset Value</th>                
                <th>Active</th>
        </thead>
        <tbody>
            <tr>
                <td><strong>Aircraft ICAO Code</strong></td>
                <?php        
                if($action == "create") 
                { ?>
                <td><input name="icao" type="text" value="<?php echo $record->icao; ?>" /> <strong>**REQUIRED**</strong></td>
                <?php
                } 
                else
                { ?>
                    <td><input type="hidden" name="icao" value="<?php echo $record->icao;?>" /><?php echo $record->icao; ?></td>
                <?php
                } ?>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 01 Speed Max (KIAS)</strong></td>
                <td><input name="flaps01_spd_max" type="text" value="<?php echo $record->flaps01_spd_max; ?>" /></td>
                <td><input name="flaps01_points" type="text" value="<?php echo $record->flaps01_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 02 Speed Max (KIAS)</strong></td>
                <td><input name="flaps02_spd_max" type="text" value="<?php echo $record->flaps02_spd_max; ?>" /></td>
                <td><input name="flaps02_points" type="text" value="<?php echo $record->flaps02_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 03 Speed Max (KIAS)</strong></td>
                <td><input name="flaps03_spd_max" type="text" value="<?php echo $record->flaps03_spd_max; ?>" /></td>
                <td><input name="flaps03_points" type="text" value="<?php echo $record->flaps03_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 04 Speed Max (KIAS)</strong></td>
                <td><input name="flaps04_spd_max" type="text" value="<?php echo $record->flaps04_spd_max; ?>" /></td>
                <td><input name="flaps04_points" type="text" value="<?php echo $record->flaps04_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 05 Speed Max (KIAS)</strong></td>
                <td><input name="flaps05_spd_max" type="text" value="<?php echo $record->flaps05_spd_max; ?>" /></td>
                <td><input name="flaps05_points" type="text" value="<?php echo $record->flaps05_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 06 Speed Max (KIAS)</strong></td>
                <td><input name="flaps06_spd_max" type="text" value="<?php echo $record->flaps06_spd_max; ?>" /></td>
                <td><input name="flaps06_points" type="text" value="<?php echo $record->flaps06_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 07 Speed Max (KIAS)</strong></td>
                <td><input name="flaps07_spd_max" type="text" value="<?php echo $record->flaps07_spd_max; ?>" /></td>
                <td><input name="flaps07_points" type="text" value="<?php echo $record->flaps07_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 08 Speed Max (KIAS)</strong></td>
                <td><input name="flaps08_spd_max" type="text" value="<?php echo $record->flaps08_spd_max; ?>" /></td>
                <td><input name="flaps08_points" type="text" value="<?php echo $record->flaps08_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 09 Speed Max (KIAS)</strong></td>
                <td><input name="flaps09_spd_max" type="text" value="<?php echo $record->flaps09_spd_max; ?>" /></td>
                <td><input name="flaps09_points" type="text" value="<?php echo $record->flaps09_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td><strong>Flaps 10 Speed Max (KIAS)</strong></td>
                <td><input name="flaps10_spd_max" type="text" value="<?php echo $record->flaps10_spd_max; ?>" /></td>
                <td><input name="flaps10_points" type="text" value="<?php echo $record->flaps10_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>            
            <tr>
                <td><strong>Landing Gear Speed Max (KIAS)</strong></td>
                <td><input name="gear_spd_max" type="text" value="<?php echo $record->gear_spd_max; ?>" /></td>
                <td><input name="gear_points" type="text" value="<?php echo $record->gear_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td><input name="gear_spd_active" type="checkbox" <?php echo ($record->gear_spd_active == 1) ? 'checked' : ''; ?> /></td>
            </tr>
            <tr>
                <td><strong>Minimum Landing Fuel (lbs)</strong></td>
                <td><input name="fuel_land_min" type="text" value="<?php echo $record->fuel_land_min; ?>" /></td>
                <td><input name="fuel_land_points" type="text" value="<?php echo $record->fuel_land_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td><input type="hidden" name="fuel_land_active" value="0" /><input name="fuel_land_active" type="checkbox" <?php echo ($record->fuel_land_active == 1) ? 'checked' : ''; ?> /></td>
            </tr>
            <tr>
                <td>
                    <strong>Landing Rate - Soft</strong>
                    <p> A landing rate between 0 and the low value will activate the points.  Should be a negative integer. Based on vertical speed FPM.
                    (Value can not be changed)</p> 
                </td>
                <td>0</td>
                <td><input name="landing_soft_points" type="text" value="<?php echo $record->landing_soft_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <strong>Landing Rate - Low</strong>
                    <p> A landing rate between this value and the middle value will activate the points.  MUST be a negative integer. Based on vertical speed FPM.
                    (Should be less than middle value)</p> 
                </td>
                <td><input name="landing_low" type="text" value="<?php echo $record->landing_low; ?>" /></td>
                <td><input name="landing_low_points" type="text" value="<?php echo $record->landing_low_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <strong>Landing Rate - Middle</strong>
                    <p> A landing rate between this value and the high value will activate the points.  MUST be a negative integer. Based on vertical speed FPM.
                    (Should be greater than low value and less than high value)</p> 
                </td>
                <td><input name="landing_mid" type="text" value="<?php echo $record->landing_mid; ?>" /></td>
                <td><input name="landing_mid_points" type="text" value="<?php echo $record->landing_mid_points; ?>" /></td
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <strong>Landing Rate - High</strong>
                    <p> A landing rate greater this value will activate the points.  MUST be a negative integer. Based on vertical speed FPM. 
                        (ex: if this is set to -1000 then a landing rate of -1100 will trigger points)</p> 
                </td>
                <td><input name="landing_high" type="text" value="<?php echo $record->landing_high; ?>" /></td>
                <td><input name="landing_high_points" type="text" value="<?php echo $record->landing_high_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>  
            <tr>
                <td>
                    <strong>Maximum Weight (lbs) - TakeOff</strong>
                    <p> If the aircraft takeoff weight is greater than this setting, the penalty will trigger. (must be positive
                    integer and in pounds.  Client will convert)</p> 
                </td>
                <td><input name="max_weight_takeoff" type="text" value="<?php echo $record->max_weight_takeoff; ?>" /></td>
                <td><input name="max_weight_takeoff_points" type="text" value="<?php echo $record->max_weight_takeoff_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <input type="hidden" name="max_weight_takeoff_active" value="0" />
                    <input name="max_weight_takeoff_active" type="checkbox" <?php echo ($record->max_weight_takeoff_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>   
            <tr>
                <td>
                    <strong>Maximum Weight (lbs) - Landing</strong>
                    <p> If the aircraft landing weight is greater than this setting, the penalty will trigger. (must be positive
                    integer and in pounds.  Client will convert)</p> 
                </td>
                <td><input name="max_weight_landing" type="text" value="<?php echo $record->max_weight_landing; ?>" /></td>
                <td><input name="max_weight_landing_points" type="text" value="<?php echo $record->max_weight_landing_points; ?>" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <input type="hidden" name="max_weight_landing_active" value="0" />
                    <input name="max_weight_landing_active" type="checkbox" <?php echo ($record->max_weight_landing_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  
            <tr>
                <td>
                    <strong>Taxi Speed</strong>
                    <p> Taxi Speed. Taxiing is determined on the status of the landing lights. If the landing lights are OFF 
                        then it is assumed that the aircraft is taxiing. (Setting should be a positive integer)</p> 
                </td>
                <td><input name="taxi_speed_value" type="text" value="<?php echo $record->taxi_speed_value; ?>" /></td>                
                <td><input name="taxi_speed_points" type="text" value="<?php echo $record->taxi_speed_points; ?>" /></td>
                <td><input name="taxi_speed_delay" type="text" value="<?php echo $record->taxi_speed_delay; ?>" /></td>                
                <td><input name="taxi_speed_repeat" type="checkbox" <?php echo ($record->taxi_speed_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td><input name="taxi_speed_reset" type="text" value="<?php echo $record->taxi_speed_reset; ?>" /></td>
                <td>
                    <input type="hidden" name="taxi_speed_active" value="0" />
                    <input name="taxi_speed_active" type="checkbox" <?php echo ($record->taxi_speed_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  
            <tr>
                <td>
                    <strong>Light Navigation</strong>
                    <p> Navigation Lights OFF while in air. (TRUE/FALSE) Triggered if the aircraft is in the air and the navigation lights are not on.</p> 
                </td>
                <td></td>                
                <td><input name="lite_navigation_points" type="text" value="<?php echo $record->lite_navigation_points; ?>" /></td>
                <td><input name="lite_navigation_delay" type="text" value="<?php echo $record->lite_navigation_delay ?>" /></td>
                <td><input name="lite_navigation_repeat" type="checkbox" <?php echo ($record->lite_navigation_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td></td>
                <td>
                    <input type="hidden" name="lite_navigation_active" value="0" />
                    <input name="lite_navigation_active" type="checkbox" <?php echo ($record->lite_navigation_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  
            <tr>
                <td>
                    <strong>Light Strobe</strong>
                    <p> Strobe Lights OFF while in air. (TRUE/FALSE) Triggered if the aircraft is in the air and the strobe lights are not on.</p> 
                </td>
                <td></td>                
                <td><input name="lite_strobe_points" type="text" value="<?php echo $record->lite_strobe_points; ?>" /></td>
                <td><input name="lite_strobe_delay" type="text" value="<?php echo $record->lite_strobe_delay; ?>" /></td>
                <td><input name="lite_strobe_repeat" type="checkbox" <?php echo ($record->lite_strobe_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td></td>
                <td>
                    <input type="hidden" name="lite_strobe_active" value="0" />
                    <input name="lite_strobe_active" type="checkbox" <?php echo ($record->lite_strobe_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>   
            <tr>
                <td>
                    <strong>Light Beacon</strong>
                    <p> Beacon Lights OFF while in air. (TRUE/FALSE) Triggered if the aircraft is in the air and the beacon lights are not on.</p> 
                </td>
                <td></td>                
                <td><input name="lite_beacon_points" type="text" value="<?php echo $record->lite_beacon_points; ?>" /></td>
                <td><input name="lite_beacon_delay" type="text" value="<?php echo $record->lite_beacon_delay; ?>" /></td>
                <td><input name="lite_beacon_repeat" type="checkbox" <?php echo ($record->lite_beacon_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td></td>
                <td>
                    <input type="hidden" name="lite_beacon_active" value="0" />
                    <input name="lite_beacon_active" type="checkbox" <?php echo ($record->lite_beacon_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  
            <tr>
                <td>
                    <strong>Light Landing Up</strong>
                    <p> Landing Lights ON above X. If the landing lights are ON above the setting value the penalty is triggered.</p> 
                </td>
                <td><input name="lite_landing_up_value" type="text" value="<?php echo $record->lite_landing_up_value; ?>" /></td>                
                <td><input name="lite_landing_up_points" type="text" value="<?php echo $record->lite_landing_up_points; ?>" /></td>
                <td></td>
                <td><input name="lite_landing_up_repeat" type="checkbox" <?php echo ($record->lite_landing_up_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td><input name="lite_landing_up_reset" type="text" value="<?php echo $record->lite_landing_up_reset; ?>" /></td>                
                <td>
                    <input type="hidden" name="lite_landing_up_active" value="0" />
                    <input name="lite_landing_up_active" type="checkbox" <?php echo ($record->lite_landing_up_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  
            <tr>
                <td>
                    <strong>Light Landing Down</strong>
                    <p> Landing Lights OFF below X. If the landing lights are OFF below the setting value the penalty is triggered.</p> 
                </td>
                <td><input name="lite_landing_down_value" type="text" value="<?php echo $record->lite_landing_down_value; ?>" /></td>                
                <td><input name="lite_landing_down_points" type="text" value="<?php echo $record->lite_landing_down_points; ?>" /></td>
                <td></td>
                <td><input name="lite_landing_down_repeat" type="checkbox" <?php echo ($record->lite_landing_down_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td><input name="lite_landing_down_reset" type="text" value="<?php echo $record->lite_landing_down_reset; ?>" /></td>                
                <td>
                    <input type="hidden" name="lite_landing_down_active" value="0" />
                    <input name="lite_landing_down_active" type="checkbox" <?php echo ($record->lite_landing_down_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  
            <tr>
                <td>
                    <strong>Reverse Thrust</strong>
                    <p> Monitor engines for reverse thrust.  If the reverse thrust is active when below the setting value the penalty will trigger.</p> 
                </td>
                <td><input name="reverse_thrust_value" type="text" value="<?php echo $record->reverse_thrust_value; ?>" /></td>                
                <td><input name="reverse_thrust_points" type="text" value="<?php echo $record->reverse_thrust_points; ?>" /></td>
                <td><input name="reverse_thrust_delay" type="text" value="<?php echo $record->reverse_thrust_delay; ?>" /></td>                
                <td><input name="reverse_thrust_repeat" type="checkbox" <?php echo ($record->reverse_thrust_repeat == 1) ? 'checked' : ''; ?> /></td>
                <td><input name="reverse_thrust_reset" type="text" value="<?php echo $record->reverse_thrust_reset; ?>" /></td>
                <td>
                    <input type="hidden" name="reverse_thrust_active" value="0" />
                    <input name="reverse_thrust_active" type="checkbox" <?php echo ($record->reverse_thrust_active == 1) ? 'checked' : ''; ?> />
                </td>
            </tr>  

            <tr>
                <td><strong>Profile Active?</strong></td>
                <?php $checked = ($record->active == 1 || !$record) ? 'CHECKED' : ''; ?>
                <td><input type="checkbox" id="enabled" name="active" value="1" <?php echo $checked ?> /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>  
            <tr>
                <td><input type="hidden" name="id" value="<?php echo $record->id;?>" /></td>
                <td><input type="hidden" name="action" value="<?php echo $action;?>" /></td>
                <td><input type="submit" name="submit" value="<?php echo $title;?>" /></td>
            </tr>
        </tbody>        
    </table>
</form>

