<?php

/*
 * kVAC Data
 * By: Jeffrey Kobus
 * www.fs-products.net
 * 02/21/2018
 * v1.0.8.0
 */

class kVACData extends CodonData
{
    protected static $setting_TableName = 'kvac_settings';
    
    public static function getSettings()
    {
        self::checkDB_Settings();

        $sql = 'SELECT *
                    FROM ' . TABLE_PREFIX . self::$setting_TableName . '
                        ORDER BY `id` ASC
                    ';
        $res = DB::get_results($sql);
        return $res;
        
    }
    
    public static function getSetting($name = NULL)
    {
        self::checkDB_Settings();
        $name = DB::escape($name);

        $sql = 'SELECT value
			FROM ' . TABLE_PREFIX . self::$setting_TableName . '
			WHERE `name` = \'' . $name . '\'
			';
        $res = DB::get_row($sql);
        return $res->value;
    }
    
    public static function saveSetting($name, $value)
    {
        self::checkDB_Settings();

        $name = strtoupper(DB::escape($name));
        $value = DB::escape($value);

        $sql = 'UPDATE ' . TABLE_PREFIX . self::$setting_TableName .
			' SET
				value=\'' . $value . '\'
			WHERE name=\'' . $name . '\'';

        $res = DB::query($sql);

        if (DB::errno() != 0)
            return false;
        return true;
    }

    public static function resetTable_Settings()
    {
        $table_name =  TABLE_PREFIX . self::$setting_TableName;
        $sql = "DROP TABLE IF EXISTS `$table_name`";
        DB::query($sql);
        self::checkDB_Settings();
    }

    public static function checkDB_Settings()
    {
        $table_name =  TABLE_PREFIX . self::$setting_TableName;

        $sql = 'SELECT * FROM ' . $table_name;
        
        $res = DB::get_results($sql);

        if (count($res) > 1)
            return;

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL,
				`title` text NOT NULL,
				`name` varchar(255) NOT NULL,
				`value` varchar(256) NOT NULL,
				`description` text NOT NULL,
				`type` varchar(4) NOT NULL
			)
			ENGINE=MyISAM;				
			";

        DB::query($sql);

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");

        self::installRows_Settings();
    }

    public static function installRows_Settings($auto = true)
    {
        $table_name =  TABLE_PREFIX . self::$setting_TableName;
        
        $sql = 'SELECT * FROM ' . $table_name;
        
        $res = DB::get_results($sql);

        if (count($res) > 1)
            return;

        $sql = "		
		INSERT IGNORE INTO `$table_name` (`id`, `title`, `name`, `value`, `description`, `type`) 
                    VALUES
                    (1, 'Enabled', 'KVAC_ENABLED', '1',
			'Is the kVAC connection enabled?', 'BOOL'),
                    (2, 'API Key', 'KVAC_APIKEY', '',
			'Create an account at kvac.fs-products.net and sign up for an API Key.', 'TEXT'),
                    (3, 'Send Positions', 'KVAC_POSITION', '1',
			'Allow live position reports to be sent to kVAC.', 'BOOL'),
                    (4, 'Send PIREP', 'KVAC_PIREP', '1',
			'Send PIREP to kVAC after filing with the VA.', 'BOOL');
		";

        DB::query($sql);
    }
}

