<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<h3>kACARSII Scoring System</h3>

<p>If the scoring system option has been purchased you can modify how the option works using these parameters.</p>

<hr>

<ul>
    <li><strong>Setting</strong> - This is the threshold for the parameter.  If the Actual value is greater than or less than (depending on parameter) the penalty will be triggered.  
        If this value is set to ZERO the parameter set will be ignored  (This does NOT include TRUE/FALSE checks. example - Overspeed).  This allows the application to run even if the DB table gets corrupted.</li>
    <li><strong>Value</strong> - This is the penalty point reduction/addition.  If the parameter is triggered this value is added to the current score.  So if the value is negative it 
        will reduce the current score by this amount.  If positive it will increase the current score by this amount.</li>
    <li><strong>Delay</strong> - This is the number of seconds that the parameters setting must be triggered for before accessing the penalty.  If set to zero the parameter will be triggered 
        as soon as the parameter is exceeded.</li>
    <li><strong>Repeat</strong> - If this is activated the parameter can be triggered again.  This works in concert with the `Reset Value`.  If not activated then the parameter can only
        be triggered once per flight.</li>
    <li><strong>Reset Value</strong> - This is used in concert with `Repeat`.  If the `Repeat` is activate this value will be used to reset the parameter so that in can be triggered again.  
        If the `Repeat` is not active this value is not used. (TRUE/FALSE checks are always reset on TRUE/FALSE transition if `Repeat` is active.)</li>
    <li><strong>Active</strong> - This determines if this parameters set is used at all.  If active then the parameter set will be monitored.  If not active the parameter set will be ignored.</li>
</ul>

<hr>

<div  style="float:right">
    <button class="{button:{icons:{primary:'ui-icon-info'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/updateScoring');?>';"> 
            Update Scoring DataBase
    </button> 
    <button class="{button:{icons:{primary:'ui-icon-alert'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/resetScoringToDefault');?>';"> 
        Reset Scoring Database (all settings will be lost)
    </button>
</div>

<table id="tabledlist" class="tablesorter">
    <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Setting</th>
            <th>Value</th>
            <th>Delay (seconds)</th>
            <th>Repeat</th>
            <th>Reset Value</th>
            <th>Active</th>
            <th>Save</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!$settings)
        {
            echo '<p>No scoring settings have been added</p>';
        }
        else
        {
            foreach ($settings as $setting)
            {
            ?>
                <tr>
                    <form id="form_<?php echo $setting->id; ?>" method="post" action="<?php echo SITE_URL ?>/admin/index.php/kacarsadmin/scoring" >
                        <td><strong><?php echo $setting->name; ?></strong></td>
                        <td><strong><?php echo $setting->description; ?></strong></td>
                        <td><?php echo '<input name="setting" value="' . $setting->setting . '" />'; ?></td>
                        <td><?php echo '<input name="value" value="' . $setting->value . '" />'; ?></td>
                        <td><?php echo '<input name="delay" value="' . $setting->delay . '" />'; ?></td>
                        <td>    
                            <?php $r_checked = ($setting->repeat==1) ? 'CHECKED' : ''; ?>
                            <input name="repeat" type="checkbox" <?php echo $r_checked ?> />
                        </td> 
                        <td><?php echo '<input name="reset" value="' . $setting->reset . '" />'; ?></td>
                        <td>    
                            <?php $a_checked = ($setting->active==1) ? 'CHECKED' : ''; ?>
                            <input name="active" type="checkbox" <?php echo $a_checked ?> />
                        </td> 
                        <td>
                            <input type="hidden" name="id" value="<?php echo $setting->id; ?>">
                            <input type="hidden" name="action" value="savesettings">
                            <input type="submit" name="submit" value="Save" />
                        </td>
                    </form>
                </tr>
            <?php
            }
        }
        ?>
    </tbody>
</table>