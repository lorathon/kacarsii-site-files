<?php

/*
 * kACARSII Admin Settings
 * By: Jeffrey Kobus
 * www.fs-products.net 
 *  02/21/2018
 * v1.0.8.0
 */

class kACARSAdmin extends CodonModule
{

    public function HTMLHead()
    {
        $this->set('sidebar', 'kacars/kacars_settings_sidebar.php');
    }

    public function NavBar()
    {
        if (PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            echo '<li><a href="' . SITE_URL . '/admin/index.php/kAcarsAdmin">kACARSII Settings</a></li>';
            
            echo '<li><a href="' . SITE_URL . '/admin/index.php/kAcarsAdmin/logsettings">kACARSII Log Settings</a></li>';
            
            if(kACARSIIData::getSetting('KACARS_CHARTER') == 1 && kACARSIIData::getSetting('KACARS_CHARTER_USER') == 1)
            {
                echo '<li><a href="' . SITE_URL . '/admin/index.php/kAcarsAdmin/charterusers">kACARSII Charter Users</a></li>';
            }
            
            if(kACARSIIData::getSetting('KACARS_AFK_SWITCH') == 1 && kACARSIIData::getSetting('KACARS_AFK_USER') == 1)
            {
                echo '<li><a href="' . SITE_URL . '/admin/index.php/kAcarsAdmin/afkusers">kACARSII AFK Users</a></li>';
            }
            
            echo '<li><a href="' . SITE_URL . '/admin/index.php/kAcarsAdmin/scoring">kACARSII Scoring System</a></li>';
            
            if(kACARSIIData::getSetting('KACARS_SCORING_AIRCRAFT') == 1)
            {
                echo '<li><a href="' . SITE_URL . '/admin/index.php/kAcarsAdmin/scoring_aircraft">kACARSII Scoring System (Aircraft Profiles)</a></li>';
            }
        }
    }

    public function index()
    {
        $this->settings();
    }

    public function settings()
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'addsetting':
                    $this->AddSetting();
                    break;
                case 'savesettings':
                    $this->saveSettingsPost();
                    break;
            }
        }
        $this->showSettings();
    }

    protected function showSettings()
    {
        $settings = kACARSIIData::getSettings();
        $this->set('settings', $settings);
        $this->render('kacars/kacars_settings_main.php');
    }

    protected function saveSettingsPost()
    {
        while (list($name, $value) = each($_POST))
        {

            if ($name == 'action')
                continue;
            elseif ($name == 'submit')
                continue;

            if ($value == 'on')
                $value = 1;
            if ($value == 'off')
                $value = 0;

            $value = DB::escape($value);

            kACARSIIData::SaveSetting($name, $value);
        }
        $this->set('message', 'kACARS Settings were saved!');
        $this->render('core_success.php');
    }

    public function resetToDefault()
    {
        kACARSIIData::resetTable_Settings();

        $this->index();
        $this->set('message', 'kACARS Settings were reset!');
        $this->render('core_success.php');
    }

    public function update()
    {
        kACARSIIData::installRows_Settings(false);

        $this->index();
        $this->set('message', 'kACARS Settings were updated!');
        $this->render('core_success.php');
    }
    
    public function afkusers($pilotid = NULL)
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'addafkuser':
                    $this->addafkuser($this->post->pilotid);
                    break;
                case 'deleteafkuser':
                    $this->deleteafkuser($pilotid);
                    break;
            }
        }
        
        $pilots = PilotData::getAllPilots();
        $afkpilots = kACARSIIData::getAfkUsers();
        $this->set('pilots', $pilots);
        $this->set('afkpilots', $afkpilots);
        $this->render('kacars/kacars_afk_users.php');        
    }
    
    public function addafkuser($pilotid)
    {
        if(intval($pilotid == 0))
        {
            $this->set('message', 'kACARS AFK User was not saved!');
            $this->render('core_error.php');
        }
        else
        {
            kACARSIIData::addAfkUser($pilotid);  
            $this->set('message', 'kACARS AFK User was saved!');
            $this->render('core_success.php');
        }       
    }
    
    public function deleteafkuser($pilotid = 0)
    {
        if(intval($pilotid == 0))
        {
            $this->set('message', 'kACARS AFK User was not deleted!');
            $this->render('core_error.php');
        }
        else
        {
            kACARSIIData::deleteAfkUser($pilotid);        
            $this->set('message', 'kACARS AFK User was deleted!');
            $this->render('core_success.php');
        }       
        $this->afkusers();
    } 


    
    
    public function scoring()
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'savesettings':
                    $this->saveScoringPost();
                    break;
            }
        }
        $this->showScoring();
    }

    protected function showScoring()
    {
        $settings = kACARSIIData::getScoring();
        $this->set('settings', $settings);
        $this->render('kacars/kacars_scoring_main.php');
    }

    public function saveScoringPost()
    {                
        if(isset($this->post->active))
            $active = true;
        else
            $active = false;
        
        if(isset($this->post->repeat))
            $repeat = true;
        else
            $repeat = false;
        
        kACARSIIData::updateScoring(
                $this->post->id, 
                $this->post->setting, 
                $this->post->value, 
                $this->post->delay,
                $repeat,
                $this->post->reset,
                $active
                );        
        
        $this->set('message', 'kACARS Scoring System setting was saved!');
        $this->render('core_success.php');
    }

    public function resetScoringToDefault()
    {        
        kACARSIIData::resetTable_Scoring();
        
        $this->set('message', 'kACARS Scoring System was reset!');
        $this->render('core_success.php');
        $this->scoring();
    }

    public function updateScoring()
    {
        kACARSIIData::installRows_Scoring(false);
        
        $this->set('message', 'kACARS Scoring System was updated!');
        $this->render('core_success.php');
        $this->scoring();
    }
    
    
    
    public function scoring_aircraft()
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'create':
                    $this->saveScoringAircraftPost();
                    break;
                case 'edit':
                    $this->saveScoringAircraftPost();
            }
        }
        
        $this->showScoringAircraft();
    }

    protected function showScoringAircraft()
    {
        $records = kACARSIIData::getScoring_AircraftAll(false);        
        $this->set('records', $records);
        $this->render('kacars/kacars_scoring_aircraft_main.php');
    }
    
    public function createScoringAircraft() 
    {        
        $this->checkPermission(FULL_ADMIN);
        $this->set('title', 'Add Scoring Aircraft Profile');
        $this->set('action', 'create');
        $this->render('kacars/kacars_scoring_aircraft_form.php');
    }
    
    public function editScoringAircraft() 
    {        
        $this->checkPermission(FULL_ADMIN);
        $this->set('title', 'Edit Scoring Aircraft Profile');
        $this->set('action', 'edit');
        $this->set('record', kACARSIIData::getScoring_Aircraft($this->get->icao));
        $this->render('kacars/kacars_scoring_aircraft_form.php');
    }
    
    public function deleteScoringAircraft($id)
    {
        kACARSIIData::deleteScoring_Aircraft($id);
        $this->scoring_aircraft();
    }

    protected function saveScoringAircraftPost()
    {                
       $this->checkPermission(FULL_ADMIN);
       
       $this->post->icao = strtoupper($this->post->icao);

        if ($this->post->icao == '') 
        {
            $this->set('message', 'You must fill out the ICAO fields.');
            $this->render('core_error.php');
            return;
        }

        if (kACARSIIData::getScoring_Aircraft($this->post->icao) && $this->post->action == "create") 
        {
            $this->set('message', 'An aircraft profile with this ICAO already exists!');
            $this->render('core_error.php');
            return;
        }
        
        $fields = array(); 
        
        foreach($this->post as $key => $value)
        {
            if($key == "action")
                continue;
            
            if($key == "id")
                continue;
            
            if($key == "submit")
                continue;
            
            if($key == "icao")
                $value = strtoupper($value);
            
            if (strpos($key, 'active') !== false)
            {
                if($value == 'on')
                    $value = 1;
            }
            
            if (strpos($key, 'repeat') !== false)
                $value = 1;
            
            if(empty($value))
                $value = 0;
            
            $fields[$key] = $value;
        }
                
        if(! isset($this->post->active))
            $fields['active'] = 0;       
        
        if($this->post->action == "create")
            kACARSIIData::createScoring_Aircraft($fields);
        else
        {
            kACARSIIData::editScoring_Aircraft($this->post->id, $fields);
        }

        if (DB::errno() != 0) 
        {
            if (DB::errno() == 1062)
            {
                $this->set('message', 'This aircraft profile has already been added.');
            }
            else  
            {
                $this->set('message', 'There was an error adding the aircraft profile.' . DB::$error);
            }

            $this->render('core_error.php');
            return;
        }

        $this->set('message', 'Added/Edited the aircraft profile - ' . $this->post->icao);
        $this->render('core_success.php');

        LogData::addLog(Auth::$userinfo->pilotid, 'Added/Edited the aircraft profile - ' . $this->post->icao);
    }
    
    public function reset_DataBase_AircraftProfile()
    {
        kACARSIIData::resetTable_Scoring_Aircraft();
        
        $this->set('message', 'kACARS Aircraft Profile Database was reset!');
        $this->render('core_success.php');
        $this->scoring_aircraft();
        
    }
    
    public function update_DataBase_AircraftProfile()
    {
        kACARSIIData::updateTable_Scoring_Aircraft();
        
        $this->set('message', 'kACARS Aircraft Profile Database was updated!');
        $this->render('core_success.php');
        $this->scoring_aircraft();
    }
    
    
    
    

    
    
    
    
    
    public function logsettings()
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'savesettings':
                    $this->saveLogSettingsPost();
                    break;
            }
        }
        $this->showLogSettings();
    }

    protected function showLogSettings()
    {
        $settings = kACARSIIData::getLogSettings();
        $this->set('settings', $settings);
        $this->render('kacars/kacars_logsettings_main.php');
    }

    protected function saveLogSettingsPost()
    {
        while (list($name, $value) = each($_POST))
        {
            if ($name == 'action')
                continue;
            elseif ($name == 'submit')
                continue;

            if ($value == 'on')
                $value = 1;
            if ($value == 'off')
                $value = 0;

            $value = DB::escape($value);

            kACARSIIData::SaveLogSetting($name, $value);
        }
        $this->set('message', 'kACARS Log Settings were saved!');
        $this->render('core_success.php');
    }

    public function resetLogToDefault()
    {
        kACARSIIData::resetTable_LogSettings();        
        $this->set('message', 'kACARS Log Settings were reset!');
        $this->render('core_success.php');
        $this->logsettings();
    }

    public function updateLog()
    {
        kACARSIIData::installRows_LogSettings(false);
        $this->set('message', 'kACARS Log Settings were updated!');
        $this->render('core_success.php');
        $this->logsettings();
    }
    
    
    
    public function charterusers($pilotid = NULL)
    {
        if (!PilotGroups::group_has_perm(Auth::$usergroups, FULL_ADMIN))
        {
            $this->set('message', 'Access Denied');
            $this->render('core_error.php');
            return;
        }
        if (isset($this->post->action))
        {
            switch ($this->post->action)
            {
                case 'addcharteruser':
                    $this->addcharteruser($this->post->pilotid);
                    break;
                case 'deletecharteruser':
                    $this->deletecharteruser($pilotid);
                    break;
            }
        }
        
        $pilots = PilotData::getAllPilots();
        $charterpilots = kACARSIIData::getCharterUsers();
        $this->set('pilots', $pilots);
        $this->set('charterpilots', $charterpilots);
        $this->render('kacars/kacars_charter_users.php');        
    }
    
    public function addcharteruser($pilotid)
    {
        if(intval($pilotid == 0))
        {
            $this->set('message', 'kACARS Charter User was not saved!');
            $this->render('core_error.php');
        }
        else
        {
            kACARSIIData::addCharterUser($pilotid);  
            $this->set('message', 'kACARS Charter User was saved!');
            $this->render('core_success.php');
        }       
    }
    
    public function deletecharteruser($pilotid = 0)
    {
        if(intval($pilotid == 0))
        {
            $this->set('message', 'kACARS Charter User was not deleted!');
            $this->render('core_error.php');
        }
        else
        {
            kACARSIIData::deleteCharterUser($pilotid);        
            $this->set('message', 'kACARS Charter User was deleted!');
            $this->render('core_success.php');
        }       
        $this->afkusers();
    } 
}
