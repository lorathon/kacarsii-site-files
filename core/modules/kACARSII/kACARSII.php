<?php

/*
 * kACARSII Module
 * By: Jeffrey Kobus
 * www.fs-products.net
 * 10/23/2018
 * v1.0.7.0
 */

class kACARSII extends CodonModule
{
    public function index()
    {       
        Config::Set('PIREP_TIME_CHECK', '5');
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST')
        {
            $postText = file_get_contents('php://input');
            $rec_xml = $postText;
            $xml = simplexml_load_string($rec_xml);

            if (!is_numeric($xml->verify->pilotID))
            {
                /* See if they are a valid pilot */
                preg_match('/^([A-Za-z]*)(\d*)/', $xml->verify->pilotID, $matches);
                $pilot_code = $matches[1];
                $pilotid = intval($matches[2]) - Config::Get('PILOTID_OFFSET');
            }
            else
            {
                $pilotid = $xml->verify->pilotID;
            }

            $pilotinfo = PilotData::getPilotData($pilotid);
            $pilotcode = PilotData::getPilotCode($pilotinfo->code, $pilotinfo->pilotid);

            /* Check for session key if not logging in */
            if ($xml->switch->data != "login")
            {
                if (!kACARSIIData::checkSession($pilotid, $xml->switch->sessionkey))
                {
                    $params = array(
                        'status' => 9,
                        'message' => 'Bad Session Key'
                    );

                    $send = self::sendXML($params);
                    return;
                }
            }

            switch ($xml->switch->data)
            {
                case login:
                    
                    if($pilotinfo->retired == '1')
                    {
                        $params = array(
                            'loginStatus' => '2'
                        );
                        self::sendXML($params);
                        return;
                    }     
                    
                    if (filter_var($xml->verify->pilotID, FILTER_VALIDATE_EMAIL))
                    {
                        $params = array(
                            'status' => 9,
                            'message' => 'Use PilotID not email.'
                        );  
                        self::sendXML($params);
                        return;
                    }
                    
                    $results = Auth::ProcessLogin($xml->verify->pilotID, $xml->verify->password);
                    if ($results)
                    {
                        $params = self::getPilotinfo($pilotinfo);

                        /* Load Settings into object */
                        $settings = kACARSIIData::getSettings();
                        if (count($settings) > 1)
                        {
                            foreach ($settings as $setting)
                                $params->{$setting->name} = $setting->value;
                        }
                        
                        /* Load Log Settings into object */
                        $settings = kACARSIIData::getLogSettings();
                        if (count($settings) > 1)
                        {
                            foreach ($settings as $setting)
                                $params->{$setting->name} = $setting->value;
                        }
                                                
                        /* AFK is ON and is set based on pilot */
                        if(kACARSIIData::getSetting('KACARS_AFK_SWITCH') == 1 && kACARSIIData::getSetting('KACARS_AFK_USER') == 1)
                        {
                            if(! kACARSIIData::getAfkUser($pilotid))
                            {
                                $params->KACARS_AFK_SWITCH = 0;
                            }
                        }  
                        /* Charter is ON and is set based on pilot */
                        if(kACARSIIData::getSetting('KACARS_CHARTER') == 1)
                        {
                            if(kACARSIIData::getSetting('KACARS_CHARTER_USER') == 1)
                            {
                                if(kACARSIIData::getCharterUser($pilotid))
                                {
                                    $params->KACARS_CHARTER = 1;
                                }
                                else
                                {
                                    $params->KACARS_CHARTER = 0;
                                }
                            }
                        }  
                        /* Load kVAC Settings into object */
                        $settings = kVACData::getSettings();
                        if (count($settings) > 1)
                        {
                            foreach ($settings as $setting)
                                $params->{$setting->name} = $setting->value;
                        }

                        /* Create Session Key */
                        $key = md5(microtime() . rand());
                        $params->sessionkey = $key;
                        kACARSIIData::createSession($pilotinfo->pilotid, $key);

                        unset($params->password);
                        unset($params->salt);
                    }
                    else
                    {
                        $params = array('loginStatus' => '0');
                    }
                    
                    CodonEvent::Dispatch('kACARSII_login', 'kACARSII', $xml);
                    
                    self::sendXML($params);
                    break;
                    
                case profile:                  
                    self::sendXML(self::getPilotinfo($pilotinfo));
                    break;

                case getbid:
                    /* Get the latest bid for the pilot */
                    $data = kACARSIIData::getLatestBid($pilotid);
                    $aircraft = OperationsData::getAircraftByReg($data->registration);

                    if (count($data) == 1)
                    {
                        if ($aircraft->enabled == 1)
                        {
                            $data->status = 1;
                        }
                        else
                        {
                            $data->status = 3;
                        }
                    }
                    else
                    {
                        $data->status = 2;
                    }
                    self::sendXML($data);
                    break;

                case getflight:
                    /* Get schedule based on flight number */		
                    $flightNumber = $xml->sch->flightNumber;
                    self::findFlight($flightNumber);
                    break;

                case liveupdate:
                    $lat = str_replace(",", ".", $xml->liveupdate->latitude);
                    $lon = str_replace(",", ".", $xml->liveupdate->longitude);

                    /* Get the distance remaining */
                    $arrapt = OperationsData::GetAirportInfo($xml->liveupdate->arrICAO);
                    $dist_remain = round(SchedulesData::distanceBetweenPoints($lat, $lon, $arrapt->lat, $arrapt->lng));

                    /* Estimate the time remaining */
                    if ($xml->liveupdate->groundSpeed > 0)
                    {
                        $minutes = round($dist_remain / $xml->liveupdate->groundSpeed * 60);
                        $time_remain = kACARSIIData::ConvertMinutes2Hours($minutes);
                    }
                    else
                    {
                        $time_remain = '00:00';
                    }

                    if($lat == 0 && $lon == 0)
                    {
                        if(kACARSIIData::getSetting('KACARS_CHAT_LOCATION') == 1)
                        {
                            $last = PIREPData::getLastReports($pilotid, 1);

                            if($last)
                            {
                                $airport = OperationsData::getAirportInfo($last->arricao);                                
                            }
                            else
                            {
                                $airport = OperationsData::getAirportInfo($pilotinfo->hub);
                            }

                            $lat = $airport->latitude;
                            $lon = $airport->longitude;
                        }
                    }   

                    $fields = array(
                        'pilotid'       => $pilotid,
                        'flightnum'     => $xml->liveupdate->flightNumber,
                        'pilotname'     => '',
                        'aircraft'      => $xml->liveupdate->registration,
                        'lat'           => $lat,
                        'lng'           => $lon,
                        'heading'       => $xml->liveupdate->heading,
                        'alt'           => $xml->liveupdate->altitude,
                        'gs'            => $xml->liveupdate->groundSpeed,
                        'depicao'       => $xml->liveupdate->depICAO,
                        'arricao'       => $xml->liveupdate->arrICAO,
                        'deptime'       => $xml->liveupdate->depTime,
                        'arrtime'       => '',
                        'route'         => $xml->liveupdate->route,
                        'distremain'    => $dist_remain,
                        'timeremaining' => $time_remain,
                        'phasedetail'   => $xml->liveupdate->status,
                        'online'        => $xml->liveupdate->online,
                        'messagelog'    => $xml->liveupdate->messagelog,
                        'client'        => $xml->liveupdate->source,
                    );

                    /* Other data in liveupdate XML */
                    $tas = $xml->liveupdate->tas;  // True Air Speed
                    $ias = $xml->liveupdate->ias;  // Indicated Air Speed
                    $fob = $xml->liveupdate->fob;  // Fuel on board (lbs)					

                    ACARSData::UpdateFlightData($pilotid, $fields);
                    
                    /* Insert Custom Columns */   
                    if(isset($xml->custom))
                    {            
                        foreach($xml->custom as $data)
                        {
                            foreach($data as $key => $value)
                            {
                                ACARSData::UpdateFlightData($pilotid, array($key => (string) $value));
                            }
                        }    
                    }

                    $params = array(
                        'distRemain' => $dist_remain,
                        'timeRemain' => $time_remain
                    );
                    
                    /* Fire event based on live update through kACARSII include xml data */
                    CodonEvent::Dispatch('kACARSII_liveupdate', 'kACARSII', $xml);
                    
                    self::sendXML($params);
                    break;

                case pirep:
                    $flightinfo = SchedulesData::getProperFlightNum($xml->pirep->flightNumber);
                    $code = $flightinfo['code'];
                    $flightnum = $flightinfo['flightnum'];
                    
                    /* check for duplicate */
                    if(kACARSIIData::checkDuplicate($pilotid, $code, $flightnum))
                    {
                        $params = array(
                            'pirepStatus' => '2',
                            'pirepError' => 'This PIREP was just submitted!'                           
                            );
                        self::sendXML($params);
                        return;
                    }

                    /* Make sure airports exist: If not, add them. */
                    if (!OperationsData::GetAirportInfo($xml->pirep->depICAO))
                    {
                        OperationsData::RetrieveAirportInfo($xml->pirep->depICAO);
                    }

                    if (!OperationsData::GetAirportInfo($xml->pirep->arrICAO))
                    {
                        OperationsData::RetrieveAirportInfo($xml->pirep->arrICAO);
                    }

                    /* Fuel conversion - kACARSII only reports in lbs */
                    $fuelused = $xml->pirep->fuelUsed;
                    if (Config::Get('LiquidUnit') == '0')
                    {
                        # Convert to KGs, divide by density since d = mass * volume
                        $fuelused = ($fuelused * .45359237) / .8075;
                    }
                    /* Convert lbs to gallons */
                    elseif (Config::Get('LiquidUnit') == '1')
                    {
                        $fuelused = $fuelused / 6.84;
                    }
                    /* Convert lbs to kgs */
                    elseif (Config::Get('LiquidUnit') == '2')
                    {
                        $fuelused = $fuelused * .45359237;
                    }

                    /* Position Reports as an array */
                    foreach ($xml->report as $report)
                    {
                        $positiondata['points'][] = array(
                            'name'              => (string) $report->name,
                            'time'              => (string) $report->time,
                            'lat'               => (string) $report->lat,
                            'lng'               => (string) $report->lng,
                            'alt'               => (string) $report->alt,
                            'head'              => (string) $report->head,
                            'gs'                => (string) $report->gs,
                            'phase'             => (string) $report->phase,
                            'warning'           => (string) $report->warning,
                            'warningdetail'     => (string) $report->warningdetail,
                            'fob'               => (string) $report->fob,
                            'time_fs_zulu'      => (string) $report->time_fs_zulu,
                            'time_fs_local'     => (string) $report->time_fs_local,
                            'time_rw_zulu'      => (string) $report->time_rw_zulu,
                            'time_rw_local'     => (string) $report->time_rw_local,
                        );
                    }

                    $data = array(
                        'pilotid'       => $pilotid,
                        'code'          => $code,
                        'flightnum'     => $flightnum,
                        'depicao'       => $xml->pirep->depICAO,
                        'arricao'       => $xml->pirep->arrICAO,
                        'aircraft'      => $xml->pirep->registration,
                        'flighttime'    => $xml->pirep->flightTime,
                        'submitdate'    => 'NOW()',
                        'comment'       => DB::escape($xml->pirep->comments),
                        'fuelused'      => $fuelused,
                        'route'         => $xml->pirep->route,
                        'source'        => $xml->pirep->source,
                        'load'          => $xml->pirep->load,
                        'landingrate'   => $xml->pirep->landing,
                        'rawdata'       => $positiondata,
                        'log'           => $xml->pirep->log,
                        'source'        => $xml->pirep->source
                    );

                    $ret = ACARSData::FilePIREP($pilotid, $data);
                    
                    /* Check to ensure the pirep did NOT file */
                    if($ret == false)
                        $ret = kACARSIIData::checkDuplicate($pilotid, $code, $flightnum);
                    
                    $last = PIREPData::getLastReports($pilotid, 1);

                    if ($ret == true)
                    {
                        /* PIREP Filed */
                        $params = array(
                            'pirepStatus' => '1',
                            'pirepID' => $last->pirepid
                        );
                        
                        self::sendXML($params);
                    }
                    else
                    {
                        /* There was a problem. Try Again */
                        $params = array(
                            'pirepStatus' => '2',
                            'acarsError' => 'ACARSData error',
                            'pirepError' => 'PirepData error'                            
                        ); 
                        
                        self::sendXML($params);
                        return;
                    }
                    
                    
                    /***
                     * Extra Data included in each Pirep Submittal
                     * 
                     * This data can be used in the pitep submision hook
                     * Time Format: HH:MM:SS
                     * Fuel reported in pounds ONLY
                     * Distance reported in Miles ONLY
                     */
                    $flightTime_Day             = $xml->pirep->flightTime_Day;
                    $flightTime_Dawn            = $xml->pirep->flightTime_Dawn;
                    $flightTime_Dusk            = $xml->pirep->flightTime_Dusk;
                    $flightTime_Night           = $xml->pirep->flightTime_Night;
                    $pax                        = $xml->pirep->pax;
                    $cargo                      = $xml->pirep->cargo;
                    $fuelStart                  = $xml->pirep->fuelStart;
                    $finalScore                 = $xml->pirep->finalScore;
                    $aircraftID                 = $xml->pirep->aircraftid;
                    $flightType                 = $xml->pirep->flightType;
                    $flightTime_OnGround        = $xml->pirep->flightTime_OnGround;                 // Total time of A/C on the ground
                    $flightTime_ParkingBrake    = $xml->pirep->flightTime_OnGround_ParkingBrake;    // Total time with the A/C on ground and PB is set
                    $distance_Scheduled         = $xml->pirep->distanceScheduled;                   // Scheduled distance to be flown
                    $distance_Direct            = $xml->pirep->distanceDirect;                      // Distance from takeoff point to landing point
                    $distance_Actual            = $xml->pirep->distanceActual;                      // Distance from position report to position report (total)
                    
                    /* if a scheduled flight */
                    $airlineCode        = $xml->pirep->airlineCode;
                    $flightNum          = $xml->pirep->flightNum;                    

                    /* Each Flight Log entry as an array */
                    foreach ($xml->log as $record)
                    {
                        $log[] = array(
                            'id'            => (string) $record->log_id,
                            'time_fs_zulu'  => (string) $record->log_time_fs_zulu,
                            'time_fs_local' => (string) $record->log_time_fs_local,
                            'time_rw_zulu'  => (string) $record->log_time_rw_zulu,
                            'time_rw_local' => (string) $record->log_time_rw_local,
                            'message'       => (string) $record->log_message,
                            'warning'       => (string) $record->log_warning
                        );
                    }
                    
                    /* Insert Custom Columns */   
                    if(isset($xml->custom))
                    {            
                        foreach($xml->custom as $data)
                        {
                            foreach($data as $key => $value)
                            {
                                PIREPData::updatePIREPFields($last->pirepid, array($key => (string) $value));
                            }
                        }    
                    }
                    
                    /* Insert pirep id into $xml object for hook */
                    $xml->pirepid = $last->pirepid;
                    
                    /* Fire event based on pirep filing through kACARSII include xml data */
                    CodonEvent::Dispatch('kACARSII_pirep_filed', 'kACARSII', $xml);
                    
                    kACARSIIData::createXML($last->pirepid, $rec_xml);
                    
                    break;

                case aircraft:
                    self::sendXML(kACARSIIData::getAllAircraft());
                    break;    

                case airport:
                    if(kACARSIIData::getSetting('KACARS_LIMIT_AIRPORTS') == 1)
                    {
                        self::sendXML(kACARSIIData::getAllScheduledAirports());
                    }
                    else
                    {
                        self::sendXML(kACARSIIData::getAllAirports());
                    }
                    break;
                    
                case getairport:                    
                    self::sendXML(kACARSIIData::getAirport($xml->data->searchdata));
                    break;

                case airline:
                    self::sendXML(kACARSIIData::getAllAirlines());
                    break;

                case searchSchedules:
                    $params = array(
                        'depicao' => DB::escape($xml->sch->depICAO),
                        'arricao' => DB::escape($xml->sch->arrICAO),
                        'aircraft' => DB::escape($xml->sch->aircraftID),
                    );

                    self::searchSchedules($params, $pilotinfo->ranklevel);
                    break;

                case bidFlight:

                    $biddata = kACARSIIData::getLatestBid($pilotid);
                    if ($biddata && DISABLE_BIDS_ON_BID)
                    {
                        $bidStatus = 2;
                    }
                    else
                    {
                        $ret = SchedulesData::AddBid($pilotid, $xml->sch->routeid);
                        if ($ret == true)
                            $bidStatus = 1;
                        else
                            $bidStatus = 0;
                    }

                    $params = array('bidStatus' => $bidStatus);
                    self::sendXML($params);
                    break;

                case parseRoute:
                    $dep = OperationsData::getAirportInfo($xml->route->depicao);
                    self::getRoute($dep->lat, $dep->lng, $xml->route->route);
                    break;

                case getPilots:
                    self::getActivePilots();
                    break;

                case getPMs:
                    self::sendXML(kACARSIIData::getPM($pilotid));
                    break;

                case sendPM:
                    $to = $xml->pm->pm_toid;
                    $from = $pilotid;
                    $title = $xml->pm->pm_title;
                    $msg = $xml->pm->pm_message;
                    kACARSIIData::sendPM($to, $from, $title, $msg);
                    break;

                case deletePM:
                    kACARSIIData::deletePM($xml->pm->pm_id);
                    break;

                case getMSG;
                    $message = kACARSIIData::getSetting("KACARS_MSG");
                    self::sendXML(array('KACARS_MSG' => $message));
                    break;

                case getNews:
                    self::getNews();
                    break;

                case getBids:
                    self::getBids($pilotid);
                    break;

                case deleteBid:
                    SchedulesData::removeBid($xml->sch->routeid);
                    break;

                case getLogbook:
                    /* Get all pireps associated with pilotid */
                    self::sendXML(kACARSIIData::getLogbook($pilotid, $xml->logbook->startid, kACARSIIData::getSetting(KACARS_SUPPRESS_MONEY)));
                    break;

                case chatSendMessage:
                    /* Insert message and return this message id */
                    $text = DB::escape($xml->chat->chatmessage);
                    $time = date("H:i");
                    
                    if(kACARSIIData::getSetting('KACARS_CHAT_LASTNAME') == 0)
                        $pilotinfo->lastname = substr($pilotinfo->lastname, 0, 1); 
                    
                    $message = '[' . $time . ']' . $pilotcode . ' ' . $pilotinfo->firstname . ' ' . $pilotinfo->lastname . ':  ' . $text;
                    $chatid = kACARSIIData::insertChatMessage($pilotid, $message, $time);
                    self::sendXML(array('chatid' => $chatid));
                    break;

                case chatGetMessage:
                    /* Check for any messages based on id sent */
                    kACARSIIData::getChatMessage(intval($xml->chat->chatid));
                    break;

                case onlinePilots:
                    /* Send online pilots list */
                    self::getOnlinePilots();
                    break;
                
                case scoringSystem:
                    $results = kACARSIIData::getScoring();
                    
                    if(count($results) > 0)
                    {
                        foreach($results as $row)
                        {
                            $params[$row->name . '_ACTIVE']     = $row->active;
                            $params[$row->name . '_SETTING']    = $row->setting;
                            $params[$row->name . '_VALUE']      = $row->value;
                            $params[$row->name . '_DELAY']      = $row->delay;
                            $params[$row->name . '_REPEAT']     = $row->repeat;
                            $params[$row->name . '_RESET']      = $row->reset;
                        }     
                    }  
                    self::sendXML($params);
                    break;
                    
                case scoringSystem_AircraftProfile:                    
                    $objs = kACARSIIData::getScoring_AircraftAll();                    
                    $profiles = array();
                    if($objs)
                    {
                        foreach($objs as $obj)
                        {
                            $profiles[] = json_decode($obj->json);
                        }
                    }   
                    self::sendXML($profiles);
                    break;

                default:
                    $params = array(
                        'status' => 9,
                        'message' => 'Switch does not exist - ' . $xml->switch->data,
                    );
                    self::sendXML($params);
                    
            }
        }
    }
    
    
    private static function getPilotinfo($pilotinfo)
    {
        $params = $pilotinfo;
        $params->loginStatus = 1;
        $params->pilotcode = PilotData::getPilotCode($pilotinfo->code, $pilotinfo->pilotid);
        $params->avatar = PilotData::getPilotAvatar($pilotinfo->pilotid);
        $params->rankimage = RanksData::getRankImage($pilotinfo->rank);
        $params->signature = fileurl(SIGNATURE_PATH . '/' . $params->pilotcode . '.png');
        $params->avglandingrate = kACARSIIData::GetUserLandingAverage($pilotinfo->pilotid);

        $last = PIREPData::getLastReports($pilotinfo->pilotid, 1);

        /* Load current location if needed */
        if ($last)
            $params->currentLocation = $last->arricao;
        else
            $params->currentLocation = $pilotinfo->hub;

        return $params;
    }

    
    private static function sendXML($data)
    {
        $xml = new SimpleXMLElement("<sitedata />");

        $_xml = $xml->addChild('data');

        if (count($data) > 0)
        {
            foreach ($data as $name => $value)
            {
                if (is_object($value))
                {
                    foreach ($value as $name => $value)
                        $_xml->addChild($name, htmlspecialchars($value));
                }
                else
                {
                    $_xml->addChild($name, htmlspecialchars($value));
                }
            }
        }

        header('Content-type: text/xml');
        $xml_string = $xml->asXML();
        echo $xml_string;
        return;
    }

    
    private static function searchSchedules($data, $ranklevel)
    {
        if ($data['depicao'] != "")
        {
            $params['s.depicao'] = $data['depicao'];
        }

        if ($data['arricao'] != "")
        {
            $params['s.arricao'] = $data['arricao'];
        }

        if ($data['aircraft'] != "0" && $data['aircraft'] != "")
        {
            $params['s.aircraft'] = $data['aircraft'];
        }

        $params['s.enabled'] = 1;

        $results = SchedulesData::findSchedules($params);

        if (count($results) > 0)
        {
            foreach ($results as $key => $row)
            {

                if ($row->aircraftlevel > $ranklevel)
                    unset($results[$key]);

                // uncomment the following section to show only todays flights				
                /*
                  $row->daysofweek = str_replace('7', '0', $row->daysofweek);
                  if(strpos($row->daysofweek, date('w')) === false)
                  unset($results[$key]);
                 */

                $aircraft = OperationsData::getAircraftByReg($results[$key]->registration);

                if ($aircraft->enabled == 1)
                    $results[$key]->status = 1;
                else
                    unset($results[$key]);

                if (DISABLE_SCHED_ON_BID && $row->bidid != 0)
                    unset($results[$key]);
            }
        }
        self::sendXML($results);
        return;
    }

    // Updated for 2.0
    private static function findFlight($flightNumber)
    {
        $flightnum = SchedulesData::getProperFlightNum($flightNumber);

        $params['s.code'] = $flightnum['code'];
        $params['s.flightnum'] = $flightnum['flightnum'];
        $params['s.enabled'] = 1;

        $data = SchedulesData::findSchedules($params);

        $aircraft = OperationsData::getAircraftByReg($data[0]->registration);

        if (count($data) == 1)
        {
            if ($aircraft->enabled == 1)
            {
                $data[0]->status = 1;
            }
            else
            {
                $data[0]->status = 3;
            }
        }
        else
        {
            $data = new stdClass();
            $data->status = 2;
            self::sendXML($data);
            return;
        }
        self::sendXML($data[0]);
        return;
    }

    // Updated for 2.0
    private static function getRoute($deplat, $deplng, $schedRoute)
    {
        $data = new stdClass();
        $data->deplat = $deplat;
        $data->deplng = $deplng;
        $data->route = $schedRoute;
        $results = NavData::parseRoute($data);

        self::sendXML($results);
        return;
    }

    // Updated for 2.0
    private static function getActivePilots()
    { 
        $results = PilotData::findPilots(Array('retired' => '0'));
        $i = 0;
        if ($results)
        {
            foreach ($results as $row)
            {
                $results[$i]->pilotcode = PilotData::getPilotCode($row->code, $row->pilotid);
                $results[$i]->signature = fileurl(SIGNATURE_PATH . '/' . $results[$i]->pilotcode . '.png');
                $results[$i]->avatar = PilotData::getPilotAvatar($row->pilotid);
                
                if(kACARSIIData::getSetting('KACARS_CHAT_LASTNAME') == 0)
                        $results[$i]->lastname = substr($row->lastname, 0, 1);
                
                unset($results[$i]->password);
                unset($results[$i]->salt);
                unset($results[$i]->email);
                unset($results[$i]->lastip);
                $i++;
            }
        }
        self::sendXML($results);
        return;
    }

    // Updated for 2.0
    private static function getNews()
    {
        $fsp_news = false;  // set to true if using the FS-Products FSP_News Module
        $count = '10'; // change to limit the number of news items sent to kACARS

        $results = kACARSIIData::GetAllNews($count, $fsp_news);
        $i = 0;
        if ($results)
        {
            foreach ($results as $row)
            {
                $results[$i]->postdate = date(DATE_FORMAT, $row->postdate);
                $i++;
            }
        }
        self::sendXML($results);
        return;
    }

    // Updated for 2.0
    public static function getNewsItem($newsid)
    {
        $fsp_news = false;  // set to true if using the FS-Products FSP_News Module		
        $result = kACARSIIData::GetNewsItem($newsid, $fsp_news);
        echo html_entity_decode($result->body);
    }

    // Updated for 2.0
    private static function getBids($pilotid)
    {
        $results = SchedulesData::getBids($pilotid);
        $i = 0;
        if ($results)
        {
            foreach ($results as $row)
            {
                $aircraft = OperationsData::getAircraftByReg($row->registration);
                if ($aircraft->enabled == 1)
                {
                    $results[$i]->status = 1;
                }
                else
                {
                    $results[$i]->status = 3;
                }               

                if(kACARSIIData::getSetting('KACARS_GENERATE_LOAD') == 1)
                {
                    $results[$i]->paxload = kACARSIILoadData::getPaxLoad($results[$i]->flightnum, $results[$i]->bidid);
                    $results[$i]->cargoload = kACARSIILoadData::getCargoLoad($results[$i]->flightnum, $results[$i]->bidid);
                }
                
                $i++;
            }
        }

        self::sendXML($results);
        return;
    }

    // Updated for 2.0
    private static function getOnlinePilots()
    {
        $results = ACARSData::GetACARSData();
        $i = 0;
        if ($results)
        {
            foreach ($results as $row)
            {
                $pilotinfo = PilotData::getPilotData($row->pilotid);
                $results[$i]->pilotcode = PilotData::GetPilotCode($pilotinfo->code, $pilotinfo->pilotid);
                
                if(kACARSIIData::getSetting('KACARS_CHAT_LASTNAME') == 0)
                    $pilotinfo->lastname = substr($pilotinfo->lastname, 0, 1);
                
                $results[$i]->pilotname = $pilotinfo->firstname . ' ' . $pilotinfo->lastname;                
                
                $i++;
                continue;
            }
        }
        self::sendXML($results);
        return;
    }

}
