kACARS II Readme
03/01/2017
Developed for: phpVMS
http://www.phpVMS.net
Copyright:    FS-Products
Developed by: Jeffrey Kobus
http://www.fs-products.net

*******************************************

Back end Installation
Please upload the admin & core folders onto your server. The structure tree in this zip file is correct and the files can be directly uploaded from this file.
Be sure to backup your site prior to installing any new files.

ADMIN FILES
These will install the required files to see all of the kACARSII setting in the admin portion of the phpVMS site.  

CORE FILES
These are the required files to ensure that kACARSII will communicate with phpVMS.

AIRCRAFT IMAGES
Aircraft images need to be in the format of ICAO.jpg and located in the following directory.  (The images must be approximately 120 X 80.)
/images/aircraft
Example - Saab 340 image    /images/aircraft/S340.jpg  (The ICAO code MUST match the ICAO code that is attached to the aircraft in the database).
You can also use the Imagelink column in the aircraft table.  To use this you would need to put the image url into this field.  Also set the proper setting 
the admin page of phpVMS.
********************************************

phpVMS 2.19
********************************************
Support has ended for 2.19 as of 09/25/2017.  Please update to phpVMS 5.52


OPTIONS

ROUTE MAP TEMPLATES -
Also included is a route_map.tpl.  If you replace your standard route_map.tpl with this one then all flights that are logged with this application will display position 
reports.  The route_map.tpl has some tips inside to show you want can be done with the popup bubbles.  The template may also be placed inside of the admin template folder 
so that while approving PIREPs you can see the actual flight path of the pilot.
******************************************** 


Client Installation

Run the .msi file.  Follow the instructions to install kACARS.  A shortcut will be placed on the desktop.  Click the shortcut to run kACARS.
********************************************


Requirements -
Windows installer 3.1
Windows .NET framework 4.0
FSUIPC
********************************************

Windows Vista & Windows 7
Client users running Windows Vista or Windows 7 may need to run this application using the "Run as Administrator" option.  This option must be set at the program file NOT 
at the shortcut.

