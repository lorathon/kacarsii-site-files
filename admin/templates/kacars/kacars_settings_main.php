<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<h3>kACARSII Settings</h3>

<p>Select available site options from this page. Don't forget to save!</p>

<div style="float:right">
    <button  class="{button:{icons:{primary:'ui-icon-info'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/update');?>';"> 
            Update Setting DataBase
    </button>  
    <button class="{button:{icons:{primary:'ui-icon-alert'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/resetToDefault');?>';"> 
        Reset Setting Database (all settings will be lost)
    </button>
</div>


<form id="form" method="post" action="<?php echo SITE_URL ?>/admin/action.php/kacarsadmin/settings">

    <table id="tabledlist" class="tablesorter">
        <thead>
            <tr>
                <th>Setting Name</th>
                <th>Paid Option</th>
                <th>Setting Value</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!$settings)
            {
                echo '<p>No settings have been added</p>';
            }
            else
            {
                foreach ($settings as $setting)
                {

                    if ($setting->paid == 1)
                        $paid = "X";
                    else
                        $paid = "";

                    echo '<tr>
			<td width="15%" nowrap>
			<strong>' . $setting->title . '</strong></td>
			<td align="center"><strong>' . $paid . '</strong></td>
			<td>';

                    switch ($setting->type)
                    {

                        case 'BOOL':
                            $checked = ($setting->value == 1) ? 'CHECKED' : '';
                            $unchecked = ($setting->value == 0) ? 'CHECKED' : '';
                            echo '<input type="radio" name="' . $setting->name . '" value="1" ' . $checked . '/>ON<br/>';
                            echo '<input type="radio" name="' . $setting->name . '" value="0" ' . $unchecked . '/>OFF';
                            break;

                        case 'INT':
                            echo '<select name="' . $setting->name . '">';
                            $x = 100;
                            $y = 0;
                            while ($y < $x)
                            {
                                $selected = ($setting->value == $y) ? 'selected' : '';
                                echo '<option value="' . $y . '" ' . $selected . '>' . $y . '</option>';
                                $y++;
                            }
                            echo '</select>';
                            break;
                            
                        case 'BINT':
                            echo '<select name="' . $setting->name . '">';
                            $x = 1000;
                            $y = 0;
                            while ($y < $x)
                            {
                                $selected = ($setting->value == $y) ? 'selected' : '';
                                echo '<option value="' . $y . '" ' . $selected . '>' . $y . '</option>';
                                $y++;
                            }
                            echo '</select>';
                            break;
                            
                        case 'LINT':
                            echo '<select name="' . $setting->name . '">';
                            $x = 60500;
                            $y = 0;
                            while ($y < $x)
                            {
                                $selected = ($setting->value == $y) ? 'selected' : '';
                                echo '<option value="' . $y . '" ' . $selected . '>' . $y . '</option>';
                                $y += 500;
                            }
                            echo '</select>';
                            break;

                        case 'AREA';
                            echo '<textarea cols="30" rows="2" name="' . $setting->name . '">' . $setting->value . '</textarea>';
                            break;

                        case 'TEXT':
                            echo '<input name="' . $setting->name . '" value="' . $setting->value . '" />';
                            break;

                        case 'XXXX':

                            break;
                        default:
                            echo '<input name="' . $setting->name . '" value="' . $setting->value . '" />';
                            break;
                    }

                    echo '</td><td>' . $setting->description . '</td></tr>';
                }
            }
            ?>
            <tr>	
                <td>
                    <input type="hidden" name="action" value="savesettings">
                    <input type="submit" name="submit" value="Save Settings" />
                </td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</form>