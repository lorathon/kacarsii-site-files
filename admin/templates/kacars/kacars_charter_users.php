<h3>Assign Pilot to Charter</h3>

<form id="addafkuser" method="POST" action="<?php echo adminurl('/kacarsadmin/charterusers'); ?>">
    <select name="pilotid">
        <?php
        foreach ($pilots as $pilot)
        {
            $pilotcode = PilotData::getPilotCode($pilot->code, $pilot->pilotid);
            echo '<option value="' . $pilot->pilotid . '"> ' . $pilotcode . ' ' . $pilot->firstname . ' ' . $pilot->lastname . '</option>';
        }
        ?>
    </select>
    <input type="hidden" name="action" value="addcharteruser" />
    <input type="submit" name="submit" value="Asign Charter User" />
</form>


<div id="afkuserlist">
    <h3>Pilots allowed to use Charter Flights</h3>
    <?php
    if (!$charterpilots)
    {
        echo 'No pilots are allowed to use Charter Flights!</div>';
        return;
    }
    ?>
    <table id="tabledlist" class="tablesorter">
        <thead>
            <tr>
                <th>Pilot</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($charterpilots as $charter)
            {
                $pilotcode = PilotData::getPilotCode($charter->code, $charter->pilotid);
                ?>
                <tr>
                    <td><?php echo $pilotcode . ' ' . $charter->firstname . ' ' . $charter->lastname; ?></td>
                    <td>
                        <button class="{button:{icons:{primary:'ui-icon-trash'}}}" 
                                onclick="window.location = '<?php echo adminurl('/kacarsadmin/deletecharteruser/' . $charter->pilotid); ?>';">Delete</button>
                    </td>
                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script>
    $('.charterdeleteajaxcall').live('click', function () {
        $("#charteruserlist").load($(this).attr("href"), {action: $(this).attr("action"), id: $(this).attr("id"), pilotid: $(this).attr("pilotid")},
                function (d) {
                    $('a.button, button, input[type=submit]').button();
                })
    });
</script>