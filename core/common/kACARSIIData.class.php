<?php

/*
 * kACARSII Data
 * By: Jeffrey Kobus
 * www.fs-products.net
 * 02/21/2018
 * v1.0.8.0
 */

class kACARSIIData extends CodonData
{
    /*********************************************
     * Main Settings Area
     *********************************************/

    public static function getSettings()
    {
        self::checkDB_Settings();

        $sql = 'SELECT *
                    FROM ' . TABLE_PREFIX . 'kacars_settings
                        ORDER BY `id` ASC
                    ';
        $res = DB::get_results($sql);
        return $res;
    }

    public static function getSetting($name)
    {
        self::checkDB_Settings();
        $name = DB::escape($name);

        $sql = 'SELECT value
			FROM ' . TABLE_PREFIX . 'kacars_settings
			WHERE `name` = \'' . $name . '\'
			';
        $res = DB::get_row($sql);
        return $res->value;
    }

    public static function saveSetting($name, $value)
    {
        self::checkDB_Settings();

        $name = strtoupper(DB::escape($name));
        $value = DB::escape($value);

        $sql = 'UPDATE ' . TABLE_PREFIX . 'kacars_settings
			SET
				value=\'' . $value . '\'
			WHERE name=\'' . $name . '\'';

        $res = DB::query($sql);

        if (DB::errno() != 0)
            return false;
        return true;
    }

    public static function resetTable_Settings()
    {
        $table_name = TABLE_PREFIX . "kacars_settings";
        $sql = "DROP TABLE IF EXISTS `$table_name`";
        DB::query($sql);
        self::checkDB_Settings();
    }

    public static function checkDB_Settings()
    {
        $table_name = TABLE_PREFIX . "kacars_settings";

        $sql = 'SELECT * FROM ' . $table_name;
        
        $res = DB::get_results($sql);

        if (count($res) > 1)
            return;

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL,
				`title` text NOT NULL,
				`name` varchar(255) NOT NULL,
				`value` varchar(256) NOT NULL,
				`description` text NOT NULL,
				`type` varchar(4) NOT NULL,
				`paid` tinyint(1) NOT NULL DEFAULT '0'
			)
			ENGINE=MyISAM;				
			";

        DB::query($sql);

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");

        self::installRows_Settings();
    }

    public static function installRows_Settings($auto = true)
    {
        $sql = 'SELECT *
			FROM ' . TABLE_PREFIX . 'kacars_settings
			';
        $res = DB::get_results($sql);

        if (count($res) > 1 && $auto)
            return;

        $table_name = TABLE_PREFIX . "kacars_settings";

        $sql = "		
		INSERT IGNORE INTO `$table_name` (`id`, `title`, `name`, `value`, `description`, `type`, `paid`) 
                    VALUES
                    (1, 'Version', 'KACARS_VER_NUMBER', '1.0.0.0',
			'Version control - Change this number to indicate the proper kACARSII version that users should be using.', 'TEXT', 0),				
                    (2, 'Version Allow', 'KACARS_VER_ALLOW', '1',
			'If ON, users will still be able to login when using a kACARSII version that does not match the required kACARSII version.
			if OFF, users will be stopped from logging in until kACARSII has been updated to the required version.', 'BOOL', 0),				
                    (3, 'Version Message', 'KACARS_VER_MSG',
                        'Please update your kACARASII as soon as possible',
			'Message that is sent to the user when attempting to log in using a kACARSII version that does not match the required kACARSII version.', 'AREA', 0),
                    (4, 'Use A/C Imagelink', 'KACARS_USE_AC_IMAGE', '0',
			'If ON, kACARSII will use the image_link column in the aircraft table for the aircraft image.
			If OFF, the image will be taken from http://www.yoursite.com/images/aircraft/ICAO.jpg', 'BOOL', 0),
                    (5, 'FS Time', 'KACARS_FS_TIME', '0', 'If ON, FS time will be used for all log entries.
			If OFF, the real world time of the user is used as the time stamps.', 'BOOL', 0),				
                    (6, 'UTC Time', 'KACARS_UTC_TIME', '0',
			'If ON, flight log timestamps are UTC. if OFF, flight log timestamps are local.  ', 'BOOL', 0),
                    (7, 'Log Accel Time', 'KACARS_ACCEL_TIME', '0',
			'If ON, kACARSII will count accelerated time as flight time.  If the user sets the acceleration rate to 4X, then for every real world second
			4 seconds will be counted as flighttime', 'BOOL', 0),
                    (8, 'Allow Charter', 'KACARS_CHARTER', '0', 'If ON, users will be able to fly a charter flight', 'BOOL', 0),				
                    (9, 'Allow Aircraft Selection', 'KACARS_AIRCRAFT_SELECT', '0',
			'If ON, users will be able to change the aircraft to another aircraft from the VA fleet.', 'BOOL', 0),				
                    (10, 'Multiple Landings', 'KACARS_MULTIPLE_LANDING', '0',
			'If ON, users can have multiple landings.  Each landing will be recorded in the flight log.
			But only the last landing will record the rate as the PIREP landing rate.', 'BOOL', 0),	
                    (11, 'Use Chat', 'KACARS_CHAT', '1',
			'If ON, chat system will be enabled.  If OFF, chat system will be disabled.  This will NOT effect any users currently logged in.', 'BOOL', 0),
                    (12, 'Show Online Pilot List', 'KACARS_ONLINE_PILOT', '1',
			'If ON, user list for logged in users will be displayed in kACARSII.', 'BOOL', 0),
                    (13, 'Web Browser Switch', 'KACARS_BROWSER_SWITCH', '1',
                        'If ON, web browser will be available if part of kACARSII Template', 'BOOL', 0),
                    (14, 'Web Browser URL', 'KACARS_BROWSER_URL', 'http://www.fs-products.net',
                        'URL that will load into the kACARSII web browser if part of kACARSII template and web browser switch is active', 'TEXT', 0),
                    (15, 'Condensed Flight Log', 'KACARS_CONDENSED', '0',
			'If ON, flight logs will be condensed and reduce the total number of log lines.', 'BOOL', 0),
                    (16, 'VA uses multiple airlines', 'KACARS_MULTI_AIRLINE', '0',
			'If ON, this VA has more than one airline.  This will allow the users to view the various airlines.', 'BOOL', 0),
                    (17, 'Retired user message', 'KACARS_RETIRED_MSG', 'Please contact an administrator. Your account is retired.',
			'This message is sent to users attempting to login with a retired account', 'AREA', 0),
                    (18, 'Suppress the currency symbol', 'KACARS_SUPPRESS_MONEY', '0',
			'If ON, site will not send the currency symbol to the kACARSII client', 'BOOL', 0),
                    (19, 'Limit Airports', 'KACARS_LIMIT_AIRPORTS', '1',
			'If ON, only airports that have a matching schedule will be sent.', 'BOOL', 0),
                    (20, 'Load Factor', 'KACARS_LOAD_FACTOR', '82',
                        'Percentage of load to set as a basline for the random load generated.', 'INT', 0),
                    (21, 'Load Variation', 'KACARS_LOAD_VARIATION', '5',
			'Percentage variation used with Load_Factor to create a random load', 'INT', 0),
                    (22, 'Live Update Interval', 'KACARS_LIVEUPDATE', '120',
			'Number of seconds to send a live update.  Minimum is 15 seconds.', 'BINT', 0),
                    (23, 'Fuel Display Control', 'KACARS_FUELDISPLAY_CONTROL', '1',
			'If ON, fuel display setting here will be used by ALL users.  If OFF, users can change the fuel display and fuel logging.', 'BOOL', 0),
                    (24, 'Fuel Display - KGS', 'KACARS_FUELDISPLAY_KGS', '0',
			'If ON, fuel display on kACARSII will be in kgs.  Otherwise it will be in lbs', 'BOOL', 0),
                    (25, 'Aircraft File', 'KACARS_FILE_AIRCRAFT', '0',
                        'If ON, kACARSII retain a copy of all aircraft from the database.  The user must manually reset the data when an update is necessary.
                        This should only be enabled if the aircraft are not regularly changed.', 'BOOL', 0),
                    (26, 'Airline File', 'KACARS_FILE_AIRLINE', '0',
			'If ON, kACARSII retain a copy of all airlines from the database.  The user must manually reset the data when an update is necessary.
                        This should only be enabled if the airlines are not regularly changed.', 'BOOL', 0),
                    (27, 'Airport File', 'KACARS_FILE_AIRPORT', '0',
                        'If ON, kACARSII retain a copy of all airports from the database.  The user must manually reset the data when an update is necessary.
                        This should only be enabled if the airports are not regularly changed.', 'BOOL', 0),
                    (28, 'Stop Acceleration', 'KACARS_SIM_RATE', '0',
                        'If ON, kACARSII will attempt to stop the user from using acceleration in the simulator.', 'BOOL', 0),                    
                    (29, 'Crash Detection', 'KACARS_CRASH_DETECTION', '1',
                            'If ON, kACARSII will not start flight unless crash detection realism setting is enabled. 
                            (Ignored when user is using XPlane)', 'BOOL', 0),
                    (30, 'Crash Detection Aircraft', 'KACARS_CRASH_DETECTION_AIRCRAFT', '1',
                            'If ON, kACARSII will not start flight unless crash detection with other aircraft realism setting is enabled.
                            (Ignored when user is using XPlane)', 'BOOL', 0),
                    (31, 'Unlimited Fuel', 'KACARS_UNLIMITED_FUEL', '1',
                            'If ON, kACARSII will not start flight unless unlimited fuel realism setting is disabled.', 'BOOL', 0),
                    (32, 'Stop Refuel', 'KACARS_STOP_REFUEL', '0',
                            'If ON, kACARSII will attempt to stop the user from increasing the fuel in the aircraft while recording.', 'BOOL', 0),
                    (33, 'Stop Pause', 'KACARS_STOP_PAUSE', '0',
                            'If ON, kACARSII will attempt to stop the user from pausing FS.', 'BOOL', 0),
                    (34, 'Flight Timer Parking Brake', 'KACARS_FLIGHTTIMER_PARKINGBRAKE', '0',
                            'If ON, kACARSII will start the flight time when the parking brakes are released.', 'BOOL', 0),
                    (35, 'Flight Number Search', 'KACARS_FLIGHTNUMBER_SEARCH', '1',
                            'If ON, kACARSII will allow a user to search a flight by the flight number.  If OFF, a user must bid on a flight.', 'BOOL', 0),
                    (36, 'Flight Level controlled by schedule', 'KACARS_FLIGHTLEVEL_SCHEDULE', '0',
                            'If ON, kACARSII will not allow the user to adjust the scheduled flight level.', 'BOOL', 0),
                    (37, 'Flight Level default altitude', 'KACARS_FLIGHTLEVEL_DEFAULT', '30000',
                            'This is the default flight level for all schedules that have a 0 flight level.', 'LINT', 0),
                    (38, 'Real World METAR', 'KACARS_METAR', '1',
                            'kACARSII will attempt to ge the real world METAR report for the departing/arriving airports.', 'BOOL', 0), 
                    (39, 'Real World Charts', 'KACARS_CHARTS', '1',
                            'This option will attempt to retrieve a one page airport chart image for the departing and arriving airports.  
                            The system will first search the users chart folder, if the chart is not found locally it will then search the 
                            VAs chart folder, if the image is not found on the VA site it will then attempt a pull of the chart image from 
                            an external site. (this site is limited to US airports).  If the chart has been found it will display this image 
                            to the user.', 'BOOL', 0), 
                    (40, 'Engine Requirement', 'KACARS_ENGINEREQUIRE', '1',
                            'With this active kACARSII will not allow a user to start/stop recording of a flight unless the engines are shut 
                            down.  If the engines are running the user will receive a message that the engines must be shut down prior to 
                            starting or stopping.', 'BOOL', 0), 
                    (41, 'Position Reports', 'KACARS_POSITION', '1',
                            'Position Reporting will track the exact path that the user flew during the flight.  This data is transmitted 
                            with all other PIREP data upon PIREP submission.  Once logged by the VA the position reports can be viewed on 
                            the route map attached to the PIREP report.  The option includes a standard template for phpVMS to display this 
                            data using the standard phpVMS icons.  More data is also now included with each report Fuel on Board and 
                            Timestamps.', 'BOOL', 0), 
                    (42, 'Site Message', 'KACARS_MSG', 'Welcome!',
                            'This is the site message that is sent to all kACARSII clients.
                            This can be updated at any time.  Care should be taken on the length of the message.', 'AREA', 0),
                    (43, 'Flight Divert', 'KACARS_DIVERT', '0',
                            'Allow users to divert the flight.  There is a site controlled setting for this option.  The VA can control 
                            if the user can divert to only airports in the VA database.  Or allow the user to divert to any airport 
                            (user will need to enter an airport ICAO code).  All information is logged and sent with all other PIREP 
                            data upojn PIREP submission.', 'BOOL', 0),
                    (44, 'Divert using airport list', 'KACARS_DIVERT_LIST', '0',
                            'If ON, the divert function will require the user to select an airport from the VA database.
                            If OFF, the user can enter any 4 character ICAO as the divert airport.', 'BOOL', 0),
                    (45, 'Progress Bar', 'KACARS_PROGRESSBAR', '1',
                            'This feature will display a progress bar showing the status of the current flight. (Only works with 
                            scheduled flights.  Does not work with charter flights.).', 'BOOL', 0),
                    (46, 'Use Departure Time', 'KACARS_DEPARTURETIME', '0',
                            'kACARSII will attempt to set the Sim time to match the schedule departure time', 'BOOL', 0),
                    (47, 'Set Departure Time (Zulu/UTC)', 'KACARS_DEPARTURETIME_ZULU', '1',
                            'Set this to OFF if your schedules use local time. Schedules MUST use a 24hr format ( 13:15 = 1:15 pm )', 'BOOL', 0),
                    (48, 'Set Departure Time (Delimiter)', 'KACARS_DEPARTURETIME_DELIMITER', ':',
			'Character used to delimit the schedule time. (ex -  12:30 = : | 12.30 = .)', 'TEXT', 0),
                    (49, 'Position Report Interval (On Ground)', 'KACARS_POSITION_GROUND', '30',
			'Number of seconds between position reports while the aircraft is on the ground. (Minimum of 15)', 'BINT', 0),
                    (50, 'Position Report Interval (Under 18000)', 'KACARS_POSITION_LOW', '120',
			'Number of seconds between position reports while the aircraft is in the air and below 18000 ft. (Minimum of 15)', 'BINT', 0),
                    (51, 'Position Report Interval (Above 18000)', 'KACARS_POSITION_HIGH', '300',
			'Number of seconds between position reports while the aircraft is in the air and above 18000 ft. (Minimum of 15)', 'BINT', 0),
                    (52, 'Require Route', 'KACARS_ROUTEREQUIRE', '1',
			'Flight will not start recording unless the route box has been populated.', 'BOOL', 0),
                    (53, 'Allow Inactive Aircraft', 'KACARS_ALLOWINACTIVEAIRCRAFT', '0',
			'Allow users to use an inactive aircraft as a substitute aircraft. Works with \"Allow Aircraft Selection\".', 'BOOL', 0),
                    (54, 'Allow Load Adjustment', 'KACARS_ALLOWLOADADJUSTMENT', '0',
			'Allow users to change the load on a flight.  Either Pax or Cargo based on flight type.', 'BOOL', 0),
                    (55, 'Flight Timer Engine', 'KACARS_FLIGHTTIMER_ENGINE', '0',
                            'If ON, kACARSII will only count flight time if at least one engine is running.', 'BOOL', 0),
                    (56, 'kACARSII Download Link', 'KACARS_DOWNLOAD_LINK', '',
                            'Properly formated URL for the download of the current version of kACARSII.', 'TEXT', 0),
                    (57, 'Allow auto pause', 'KACARS_AUTO_PAUSE', '0',
                            'This will allow the user to select a variable number of miles from the arrival.  kACARSII will then attempt to
                            pause the simulator at this point.', 'BOOL', 0),
                    (58, 'Charter User', 'KACARS_CHARTER_USER', '0',
			'If Charter is ON and this is ON then onloy pilots in the charter list will be allowed to use the charter system.', 'BOOL', 0),
                    (59, 'Display Full Last Name', 'KACARS_CHAT_LASTNAME', '1',
			'If ON then a pilots full name name will be displayed on the client.  If OFF then only the first letter of the last name,', 'BOOL', 0),
                    (60, 'Generate Load', 'KACARS_GENERATE_LOAD', '0',
            'If ON then the load will be generated locally rather than inside of kACARSII,', 'BOOL', 0),

                        

                    (71, 'Multiple Screenshot', 'KACARS_SS_UNIQUE', '0',
                            'If OFF, each user will only have a single screenshot image file.  Each update overwrites this file.  If ON, kACARSII will 
                            upload each screenshot with a unique name so that all files are retained.', 'BOOL', 1),
                    (72, 'FTP Address', 'KACARS_FTP_ADDRESS', '',
			'This is the FTP address for the screenshot option to upload images using.  ', 'TEXT', 1),				
                    (73, 'FTP Username', 'KACARS_FTP_USER', '',
			'FTP user that will be used by the screenshot option to upload the images.
			This user should only have access to a folder.  Do NOT allow complete access to this FTP user.', 'TEXT', 1),				
                    (74, 'FTP Password', 'KACARS_FTP_PASSWORD', '', 'FTP Password for use with the FTP Username', 'TEXT', 1),				
                    (75, 'FTP SSL', 'KACARS_FTP_SSL', '0', 'Use SSL for FTP connection', 'BOOL', 1),
                    (76, 'Position Check - Ignore', 'KACARS_POSCHECK_IGNORE', '0',
			'If ON and position check option has been purchased, user will be able to ignore the position check.  If OFF, the user
			MUST be withing X miles of the departing/arriving aiport.', 'BOOL', 1),
                    (77, 'AFK Switch', 'KACARS_AFK_SWITCH', '1',
			'If ON and AFK has been purchased, AFK will be enabled', 'BOOL', 1),                    
                    (78, 'AFK Minimum Altitude', 'KACARS_AFK_MINALT', '2000',
                            'If AFK is ON and AFK option has been purchased, it will not trigger unless the aircraft is above this altitude.', 'LINT', 1),                    
                    (79, 'AFK User', 'KACARS_AFK_USER', '0',
			'If ON and AFK has been purchased and AFK Switch is ON, AFK will only be enabled for those users in the AFK user list.', 'BOOL', 1),
                    (80, 'Landing Light Altitude Trigger', 'KACARS_LANDINGLIGHT_ALTITUDE', '10000',
                            'This is the altitude for the landing light status to be toggled.', 'LINT', 1),
                    (81, 'Scoring System', 'KACARS_SCORING', '1',
                            'Toggle scoring system ON or OFF.', 'BOOL', 1),
                    (82, 'Use Aircraft profiles in scoring option', 'KACARS_SCORING_AIRCRAFT', '1',
                            'Toggle between using the aircraft profiles or ignoring the aircraft profiles.', 'BOOL', 1),
                    (83, 'AFK - Use Random Time', 'KACARS_AFK_RANDOM', '1',
			'If ON and AFK has been purchased, AFK checks will be completely random.', 'BOOL', 1),  
                    (84, 'AFK - Initial Check', 'KACARS_AFK_INTERVAL_INITIAL', '10',
                            'If AFK is ON and AFK option has been purchased, the first check will come after this number of minutes. (Ignored if Random is set)', 'BINT', 1), 
                    (85, 'AFK - Subsequent Check', 'KACARS_AFK_INTERVAL_SUBSEQUENT', '30',
                            'If AFK is ON and AFK option has been purchased, all checks after the first will come after this number of minutes. (Ignored if Random is set)', 'BINT', 1),
                    (86, 'Google Maps API Key', 'KACARS_GOOGLEMAPSKEY', '',
                            'DEPRECTED', 'TEXT', 1),
                    (87, 'MapBox Token', 'KACARS_MAPBOXTOKEN', '',
                            'If the flight map option has been purchased this token is needed for the map to work.  This is for the Leaflet map. Get a token at https://www.mapbox.com', 'TEXT', 1);
		";

        DB::query($sql);
        
        echo DB::$error;
    }
    
    
    
    /************************************************
     * Save XML of all pirep filings
     ************************************************/
    
    public static function createXML($pirepid = null, $xml = null)
    {
        self::checkDB_XML();
        
        $pirepid = intval($pirepid);

        if (intval($pirepid) == 0)
            return false;
        
        if($xml == null)
            return false;

        $sql = "INSERT INTO " . TABLE_PREFIX . "kacars_xml (
                    `pirepid`, `xml`)
                    VALUES 
                    ( '$pirepid', '$xml')
                ";

        DB::query($sql);
        echo DB::$error;
    }
    
    public static function checkDB_XML()
    {
        $table_name = TABLE_PREFIX . "kacars_xml";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
		(
                    `id` int(11) NOT NULL,
                    `pirepid` int(11) NOT NULL,
                    `xml` longtext NOT NULL
		)
		ENGINE=MyISAM;				
		";

        DB::query($sql);

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `pirepid` (`pirepid`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");

        self::installRows_Settings();        
    }
    
    
    
    /************************************************
     * Session Control
     ************************************************/
    
    public static function checkSession($pilotid, $sessionkey)
    {
        $pilotid = intval($pilotid);
        $sessionkey = DB::escape($sessionkey);

        $sql = 'SELECT *
						FROM ' . TABLE_PREFIX . 'kacars_sessions
						WHERE `pilotid` = ' . $pilotid . '
						AND `sessionkey` = \'' . $sessionkey . '\''
        ;

        return DB::get_row($sql);
    }

    public static function createSession($pilotid, $sessionkey)
    {
        self::checkDB_Sessions();
        self::deleteSessions($pilotid);

        $pilotid = intval($pilotid);
        $sessionkey = DB::escape($sessionkey);
        $ipaddress = $_SERVER['REMOTE_ADDR'];

        if ($pilotid == 0)
            return false;

        $sql = "INSERT INTO " . TABLE_PREFIX . "kacars_sessions (
						`pilotid`, `sessionkey`, `ipaddress`, `timestamp`)
						VALUES ( '$pilotid', '$sessionkey', '$ipaddress', UTC_TIMESTAMP())"
        ;

        DB::query($sql);
        return DB::$error;
    }

    public static function updateSession($pilotid, $sessionkey, $chatting = false)
    {
        $pilotid = intval($pilotid);
        $sessionkey = DB::escape($sessionkey);
        $ipaddress = $_SERVER['REMOTE_ADDR'];

        if ($pilotid == 0)
            return false;

        $sql = 'UPDATE `' . TABLE_PREFIX . 'kacars_sessions`
						SET `ipaddress`=\'' . $ipaddress . '\', `chatting`=' . $chatting . ',`timestamp`=UTC_TIMESTAMP()
						WHERE `pilotid`=' . $pilotid . '
						AND `sessionkey` = \'' . $sessionkey . '\''
        ;



        DB::query($sql);
        return true;
    }

    public static function deleteSessions($pilotid)
    {
        $pilotid = intval($pilotid);

        $sql = "DELETE FROM " . TABLE_PREFIX . "kacars_sessions
						WHERE `pilotid`=$pilotid"
        ;

        DB::query($sql);
    }

    private static function checkDB_Sessions()
    {
        $table_name = TABLE_PREFIX . "kacars_sessions";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(  
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`pilotid` int(11) NOT NULL,
				`chatting` tinyint(1) NOT NULL DEFAULT '0',
				`sessionkey` varchar(255) DEFAULT NULL,
				`ipaddress` varchar(25) DEFAULT NULL,
				`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				PRIMARY KEY ( `id` )
			)
			ENGINE=INNODB;
			";

        DB::query($sql);
    }

    
    
    /************************************************
     * PIREP Control
     ************************************************/
    
    public static function checkDuplicate($pilotid, $code, $flightnum)
    {
        if (Config::Get('PIREP_CHECK_DUPLICATE') == true) 
        {
            $time_limit = Config::Get('PIREP_TIME_CHECK');
            
            if (empty($time_limit)) 
                $time_limit = 1;

            $sql = "SELECT `pirepid` FROM " . TABLE_PREFIX . "pireps
			WHERE `pilotid` = {$pilotid} 
			AND `code` = '{$code}'
			AND `flightnum` = '{$flightnum}' 
			AND DATE_SUB(NOW(), INTERVAL {$time_limit} MINUTE) <= `submitdate`";

            $res = DB::get_row($sql);

            if($res)
                return true;
            else
                return false;
        }
    }
   
    
    
    
    /************************************************
     * Schedule Control
     ************************************************/

    public static function getLatestBid($pilotid)
    {
        $sql = 'SELECT s.*, b.bidid, a.id as aircraftid, a.name as aircraft, a.registration, a.maxpax, a.maxcargo
			FROM ' . TABLE_PREFIX . 'schedules s, 
			 ' . TABLE_PREFIX . 'bids b,
			 ' . TABLE_PREFIX . 'aircraft a
			WHERE b.routeid = s.id 
			AND s.aircraft=a.id
			AND b.pilotid=' . intval($pilotid) . '
			ORDER BY b.bidid ASC LIMIT 1';

        $result = DB::get_row($sql);

        if(kACARSIIData::getSetting('KACARS_GENERATE_LOAD') == 1)
        {
            $results[$i]->paxload = kACARSIILoadData::getPaxLoad($result->flightnum, $result->bidid);
            $results[$i]->cargoload = kACARSIILoadData::getCargoLoad($result->flightnum, $result->bidid);
        }

        return $result;
    }



    /************************************************
     * Chat Control
     ************************************************/

    public static function insertChatMessage($pilotid, $message, $time)
    {
        self::checkDB_Chat();

        $message = DB::escape($message);

        $sql = "INSERT INTO " . TABLE_PREFIX . "kacars_acarschat (
				`pilotid`, `message`, `time`, `timestamp`)
				VALUES (
				'$pilotid', '$message', '$time', UTC_TIMESTAMP())";

        DB::query($sql);
        return DB::$insert_id;
    }

    public static function getChatMessage($id)
    {
        self::checkDB_Chat();

        $sql = 'SELECT *
				FROM ' . TABLE_PREFIX . 'kacars_acarschat
				WHERE `id` >= ' . $id . '				
				ORDER BY `id` ASC';

        $results = DB::get_results($sql);

        $xml = new SimpleXMLElement("<chat />");
        $info_xml = $xml->addChild('info');
        if ($results)
        {
            foreach ($results as $row)
            {
                $info_xml->addChild('id', $row->id);
                $info_xml->addChild('pilotid', $row->pilotid);
                $info_xml->addChild('message', stripcslashes($row->message));
                $info_xml->addChild('time', $row->time);
                $info_xml->addChild('timestamp', $row->timestamp);
            }
        }
        header('Content-type: text/xml');
        echo $xml->asXML();
    }

    private static function checkDB_Chat()
    {
        $table_name = TABLE_PREFIX . "kacars_acarschat";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`pilotid` int(11) DEFAULT NULL,
				`message` text,
				`time` text,
				`timestamp` text,
				PRIMARY KEY ( `id` )
			)
			ENGINE=MyISAM;
			";

        DB::query($sql);
    }
    
    
    
    
    /************************************************
     * Private Message Control
     ************************************************/	

    public static function getPM($pilotid)
    {
        self::checkDB_PM();

        $sql = 'SELECT *
			FROM ' . TABLE_PREFIX . 'kacars_pmlog
			WHERE `to` = ' . $pilotid . '
			AND `hide` = 0
			ORDER BY `id` ASC';

        $ret = DB::get_results($sql);
        echo DB::$error;
        return $ret;
    }

    public static function sendPM($to, $from, $title, $msg, $alert = 0)
    {
        self::checkDB_PM();

        $to = intval($to);
        $from = intval($from);
        $title = DB::escape($title);
        $msg = DB::escape($msg);
        $alert = intval($alert);

        $sql = "INSERT INTO " . TABLE_PREFIX . "kacars_pmlog (
			`to`, `from`, `title`, `msg`, `alert`, `timestamp`)
			VALUES (
			'$to', '$from', '$title', '$msg', '$alert', UTC_TIMESTAMP())";

        $res = DB::query($sql);
        echo DB::$error;
    }

    public static function deletePM($id)
    {
        $sql = 'UPDATE `' . TABLE_PREFIX . 'kacars_pmlog`
			SET `hide`=1
			WHERE `id`=' . $id;
        DB::query($sql);
        echo DB::$error;
    }

    private static function checkDB_PM()
    {
        $table_name = TABLE_PREFIX . "kacars_pmlog";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL,
				`from` int(11) DEFAULT NULL,
				`to` int(11) DEFAULT NULL,
				`title` text,
				`msg` text,
				`hide` int(1) DEFAULT '0',
				`alert` tinyint(1) NOT NULL DEFAULT '0',
				`timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
			)
			ENGINE=MyISAM
			";

        DB::query($sql);
        
        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
    }
   
    
    
    
    
    /************************************************
     * News Control
     ************************************************/

    public static function GetAllNews($count = 10, $fsp_news = false)
    {
        if (!$fsp_news)
        {
            $sql = 'SELECT
				id, subject, postedby, UNIX_TIMESTAMP(postdate) AS postdate
				FROM ' . TABLE_PREFIX . 'news 
				ORDER BY postdate DESC
				';
        }
        else
        {
            $sql = 'SELECT
				n.id, n.title, UNIX_TIMESTAMP(n.date_creation) AS postdate,
				CONCAT(p.firstname, " ", p.lastname) as postedby
				FROM ' . TABLE_PREFIX . 'fsp_news n
				LEFT JOIN ' . TABLE_PREFIX . 'pilots p ON p.pilotid = n.user_creation 
				ORDER BY postdate DESC
				';
        }

        if ($count != '')
            $sql .= ' LIMIT ' . intval($count);

        return DB::get_results($sql);
    }

    public static function GetNewsItem($id, $fsp_news = false)
    {
        $id = intval($id);

        if (!$fsp_news)
        {

            $sql = 'SELECT body
				FROM ' . TABLE_PREFIX . 'news
				WHERE id = ' . $id
            ;
        }
        else
        {

            $sql = 'SELECT body
				FROM ' . TABLE_PREFIX . 'fsp_news
				WHERE id = ' . $id
            ;
        }

        return DB::get_row($sql);
    }

    
    
    
    /*************************************************
     * Extra User Data
     *************************************************/
    
    public static function GetUserLandingAverage($pilotid)
    {        
        $sql = 'SELECT
                    count(pirepid) as flights,
                    sum(landingrate) as total
                    FROM ' . TABLE_PREFIX . 'pireps
                    WHERE pilotid = ' . intval($pilotid) . '
                    AND accepted = 1
                    ';
        
        $ret = DB::get_row($sql);
        
        if($ret->flights == 0)
            return '0';
        
        return ($ret->total / $ret->flights);    
    }
    
    
    
    
    /*************************************************
     * Logbook Control
     *************************************************/

    public static function getLogbook($pilotid, $start = 0, $currency = FALSE)
    {
        if (trim($pilotid) == '')
            return;

        if (trim($start) == '')
            $start = 0;

        $sql = 'SELECT p.*
			FROM ' . TABLE_PREFIX . 'pireps p
			WHERE p.`pilotid` = ' . intval($pilotid) . '
			AND p.`pirepid` > ' . $start . '
			ORDER BY p.`pirepid` DESC
			';

        $results = DB::get_results($sql);

        $i = 0;

        if ($results)
        {
            foreach ($results as $row)
            {
                if(! $currency)
                {                
                    $results[$i]->price = FinanceData::FormatMoney($results[$i]->price);
                    $results[$i]->fuelprice = FinanceData::FormatMoney($results[$i]->fuelprice);
                    $results[$i]->fuelunitcost = FinanceData::FormatMoney($results[$i]->fuelunitcost);
                    $results[$i]->pilotpay = FinanceData::FormatMoney($results[$i]->pilotpay);
                    $results[$i]->gross = FinanceData::FormatMoney($results[$i]->gross);
                    $results[$i]->expenses = FinanceData::FormatMoney($results[$i]->expenses);
                    $results[$i]->revenue = FinanceData::FormatMoney($results[$i]->revenue);
                }
                $results[$i]->fuelunit = Config::Get('LiquidUnit');
                unset($results[$i]->rawdata);
                $i++;
            }
        }

        return $results;
    }

    
    
    
    
    /**************************************************
     * Operations Control
     **************************************************/

    public static function getAllAircraft()
    {
        $sql = 'SELECT a.*, r.`rank`,
					COUNT(p.`pirepid`) as flights,
					SUM(p.`distance`) as distance
				FROM ' . TABLE_PREFIX . 'aircraft a
				LEFT JOIN ' . TABLE_PREFIX . 'ranks r ON r.rankid = a.minrank
				LEFT JOIN ' . TABLE_PREFIX . 'pireps p ON p.aircraft = a.id
				GROUP BY a.`id`
				ORDER BY a.`icao` ASC
				';

        $results = DB::get_results($sql);
        return $results;
    }
    
    public static function getAirport($icao)
    {
        $sql = 'SELECT * FROM ' . TABLE_PREFIX . 'airports 
                    WHERE `icao` = `'.$icao.'`';
        
        $results = DB::get_row($sql);
        return $results;
    }

    public static function getAllAirports()
    {
        $sql = 'SELECT * FROM ' . TABLE_PREFIX . 'airports 
					ORDER BY `icao` ASC';
        
        $results = DB::get_results($sql);
        return $results;
    }

    public static function getAllScheduledAirports()
    {
        $sql = 'SELECT a.*
                FROM ' . TABLE_PREFIX . 'airports a
                JOIN ' . TABLE_PREFIX . 'schedules s ON s.`depicao` = a.`icao`
                GROUP BY a.`icao`
		ORDER BY a.`icao` ASC';

        $results = DB::get_results($sql);
        return $results;
    }

    public static function getAllAirlines()
    {
        $sql = 'SELECT a.*,
                    COUNT(p.`pirepid`) as flights,
                    SUM(p.`distance`) as distance					
		FROM ' . TABLE_PREFIX . 'airlines a
		LEFT JOIN ' . TABLE_PREFIX . 'pireps p ON p.code = a.code
		GROUP BY a.`id`
		ORDER BY a.`name` ASC';

        $results = DB::get_results($sql);
        return $results;
    }
    
    
    
    /**************************************************
     * Functions / Helpers
     **************************************************/

    public static function ConvertMinutes2Hours($Minutes)
    {
        if ($Minutes < 0)
        {
            $Min = Abs($Minutes);
        }
        else
        {
            $Min = $Minutes;
        }

        $iHours = Floor($Min / 60);
        $Minutes = ($Min - ($iHours * 60)) / 100;
        $tHours = $iHours + $Minutes;

        if ($Minutes < 0)
        {
            $tHours = $tHours * (-1);
        }

        $aHours = explode(".", $tHours);
        $iHours = $aHours[0];

        if (empty($aHours[1]))
        {
            $aHours[1] = "00";
        }

        $Minutes = $aHours[1];

        if (strlen($Minutes) < 2)
        {
            $Minutes = $Minutes . "0";
        }

        $tHours = $iHours . ":" . $Minutes;
        return $tHours;
    }

    
    
    /**************************************************
     * AFK (Away From Keyboard) Control - individual users
     **************************************************/
    
    public static function getAfkUsers()
    {
        self::checkDB_AFKUser();

        $sql = 'SELECT a.*, p.*
                    FROM ' . TABLE_PREFIX . 'kacars_afk_users a
                    LEFT JOIN ' . TABLE_PREFIX . 'pilots p ON p.pilotid = a.pilotid';
                
        $res = DB::get_results($sql);        
        return $res;
    }
    
    public static function getAfkUser($pilotid)
    {
        self::checkDB_AFKUser();

        $sql = 'SELECT a.*, p.*
                    FROM ' . TABLE_PREFIX . 'kacars_afk_users a
                    LEFT JOIN ' . TABLE_PREFIX . 'pilots p ON p.pilotid = a.pilotid
                    WHERE a.pilotid = ' . $pilotid . '
                    LIMIT 1';
                
        $res = DB::get_row($sql);
        return $res;
    }
    
    public static function addAfkUser($pilotid)
    {
        self::checkDB_AFKUser();

        $pilotid = intval($pilotid);
        if ($pilotid == 0)
            return false;
        
        if(! self::getAfkUser($pilotid))
        {
            $sql = "INSERT INTO " . TABLE_PREFIX . "kacars_afk_users (
                        `pilotid`)
                        VALUES ( '$pilotid')";

            return DB::query($sql);
        }        
        return true;
    }
    
    public static function deleteAfkUser($pilotid)
    {
        $pilotid = intval($pilotid);

        $sql = "DELETE FROM " . TABLE_PREFIX . "kacars_afk_users
                    WHERE `pilotid`=$pilotid";

        DB::query($sql);        
    }
    
    public static function checkDB_AFKUser()
    {
        $table_name = TABLE_PREFIX . "kacars_afk_users";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL,
				`pilotid` int(11) NOT NULL
			)
			ENGINE=MyISAM;				
			";

        DB::query($sql);

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
    }    
    
    
    
    /****************************************************
     * Flight Logging Settings
     ****************************************************/

    public static function getLogSettings()
    {
        self::checkDB_LogSettings();

        $sql = 'SELECT *
                    FROM ' . TABLE_PREFIX . 'kacars_logsettings
                    ';
        $res = DB::get_results($sql);
        return $res;
    }

    public static function getLogSetting($name)
    {
        self::checkDB_LogSettings();
        $name = DB::escape($name);

        $sql = 'SELECT value
			FROM ' . TABLE_PREFIX . 'kacars_logsettings
			WHERE `name` = \'' . $name . '\'
			';
        $res = DB::get_row($sql);
        return $res->value;
    }

    public static function saveLogSetting($name, $value)
    {
        self::checkDB_LogSettings();

        $name = strtoupper(DB::escape($name));
        $value = DB::escape($value);

        $sql = 'UPDATE ' . TABLE_PREFIX . 'kacars_logsettings
			SET
				value=\'' . $value . '\'
			WHERE name=\'' . $name . '\'';

        $res = DB::query($sql);

        if (DB::errno() != 0)
            return false;
        return true;
    }

    public static function resetTable_LogSettings()
    {
        $table_name = TABLE_PREFIX . "kacars_logsettings";
        $sql = "DROP TABLE IF EXISTS `$table_name`";
        DB::query($sql);
        self::checkDB_LogSettings();
    }

    public static function checkDB_LogSettings()
    {
        $table_name = TABLE_PREFIX . "kacars_logsettings";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL,
				`title` text NOT NULL,
				`name` varchar(255) NOT NULL,
				`value` int(1) NOT NULL,
				`description` text NOT NULL
			)
			ENGINE=MyISAM;				
			";

        DB::query($sql);

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");

        self::installRows_LogSettings();
    }

    public static function installRows_LogSettings($auto = true)
    {
        $sql = 'SELECT *
			FROM ' . TABLE_PREFIX . 'kacars_logsettings
			';
        $res = DB::get_results($sql);

        if (count($res) > 1 && $auto)
            return;

        $table_name = TABLE_PREFIX . "kacars_logsettings";

        $sql = "		
		INSERT IGNORE INTO `$table_name` (`id`, `title`, `name`, `value`, `description`) 
                    VALUES
                    			
                    (1, 'Flaps', 'KACARS_LOG_FLAPS', '1',
			'Log all aircraft flap changes.  If the aircraft is airbourne the flap logging will also include aircraft
                        speed data.  This data can be used to determine if the user was following proper flap procedure.'), 
                    (2, 'Spoilers', 'KACARS_LOG_SPOILERS', '1',
			'This will log all spoiler changes.  If the aircraft is airbourne the spolier logging will also 
                        include aircraft speed data.  This data can be used to determine if the user was following proper 
                        spolier procedure. Unfortunately at this time the PMDG aircraft may use the spoilers during normal 
                        turns and the flight log can get spammed with spoiler data.  A solution is being worked on but the 
                        end result may be to ignore all spoiler data if the aircraft title has PMDG in it.  '), 
                    (3, 'Aircraft Lights', 'KACARS_LOG_LIGHTS', '1',
			'Log all aircraft light changes.  When logging the landing light status and the aircraft is airbourne 
                        aircraft altitude data is also logged.  Certain paid addon aircraft may not register some of the lighting 
                        changes.  This is because of the  way these aircraft handle the lights externally of FS. '),
                    (4, 'GPS / NAV', 'KACARS_LOG_GPSNAV', '1',
			'Log changes to the GPS/NAV selector switch.  Initial state of the switch is also logged.  This can be 
                        useful to track how a user is using the auto pilot of the aircraft.'), 
                    (5, 'Weather (Takeoff and Landing)', 'KACARS_LOG_WEATHER', '1',
			'This will log the current simulated weather on takeoff and landing.  The data includes wind speed, 
                        wind direction, temperatures, percepitation, etc......'), 
                    (6, 'Engines', 'KACARS_LOG_ENGINES', '1',
			'Each engines start and shutdown will be logged to the flight log.'),
                    (7, 'COM / Transponder', 'KACARS_LOG_COMTRANSPONDER', '1',
			'Log all frequency changes from the COM radions (both COM1 and COM2) and the transponder.'),
                    (8, 'NAV', 'KACARS_LOG_NAV', '1',
			'Log all frequency changes from the NAV radios (both NAV1 and NAV2).'),
                    (9, 'ADF', 'KACARS_LOG_ADF', '1',
			'Log all frequency changes from the ADF radios (both ADF1 and ADF2).'),
                    (10, 'Turbulence', 'KACARS_LOG_TURBULENCE', '1',
			'Log any turbulence changes that are encountered during the flight.  The range of turbulence 
                        (NONE, LIGHT, MODERATE, HEAVY, SEVERE).'),
                    (11, 'Runway Condition', 'KACARS_LOG_RUNWAYCONDITION', '1',
			'This will log the takeoff and landing runway condition.  The condition would be the surface 
                        and condition. Surface: what is the runway made of grass, dirt, concrete, etc...  
                        Condition: Wet, Icy, etc.....'),
                    (12, 'Closest Airport', 'KACARS_LOG_CLOSESTAIRPORT', '1',
			'Log the closest FS airport during takeoff and landing.  This data is retrieved from the VA airport database.'),
                    (13, 'Runway and Gate', 'KACARS_LOG_RUNWAYGATE', '1',
			'Log the departure/arrival gate and departure and arrival runway.'),
                    (14, 'Aircraft Weight', 'KACARS_LOG_AIRCRAFTWEIGHT', '1',
			'Log the weight of the aircraft on take off and landing.  Units will be based on fuel display selection.'),                        
                    (15, 'Lights (Navigation)', 'KACARS_LOG_LIGHTS_NAV', '1',
			'If logging of aircraft lights is ON and this setting is ON, Navigation light changes will be logged.'),
                    (16, 'Lights (Beacon)', 'KACARS_LOG_LIGHTS_BEACON', '1',
			'If logging of aircraft lights is ON and this setting is ON, Beacon light changes will be logged.'),
                    (17, 'Lights (Landing)', 'KACARS_LOG_LIGHTS_LANDING', '1',
			'If logging of aircraft lights is ON and this setting is ON, Landing light changes will be logged.'),
                    (18, 'Lights (Taxi)', 'KACARS_LOG_LIGHTS_TAXI', '1',
			'If logging of aircraft lights is ON and this setting is ON, Taxi light changes will be logged.'),
                    (19, 'Lights (Strobe)', 'KACARS_LOG_LIGHTS_STROBE', '1',
			'If logging of aircraft lights is ON and this setting is ON, Strobe light changes will be logged.'),
                    (20, 'Lights (Instrument)', 'KACARS_LOG_LIGHTS_INSTRUMENT', '1',
			'If logging of aircraft lights is ON and this setting is ON, Instrument light changes will be logged.'),
                    (21, 'Lights (Recognition)', 'KACARS_LOG_LIGHTS_RECOGNITION', '1',
			'If logging of aircraft lights is ON and this setting is ON, Recognition light changes will be logged.'),
                    (22, 'Lights (Wing)', 'KACARS_LOG_LIGHTS_WING', '1',
			'If logging of aircraft lights is ON and this setting is ON, Wing light changes will be logged.'),
                    (23, 'Lights (Logo)', 'KACARS_LOG_LIGHTS_LOGO', '1',
			'If logging of aircraft lights is ON and this setting is ON, Logo light changes will be logged.'),
                    (24, 'Lights (Cabin)', 'KACARS_LOG_LIGHTS_CABIN', '1',
			'If logging of aircraft lights is ON and this setting is ON, Cabin light changes will be logged.'),
                    (25, 'Fuel Control', 'KACARS_LOG_FUEL_CONTROL', '0',
			'If ON then any changes that kACARSII makes to the fuel tanks will be logged.  This can cause the flight 
                        log to be spammed with some aircraft.'),
                    (26, 'View Mode', 'KACARS_LOG_VIEWMODE', '1',
			'If ON then kACARSII will log the view mode of the simulator on take off and landing.'),
                    (27, 'Engine Count and Type', 'KACARS_LOG_ENGINES_COUNT', '1',
			'If ON then kACARSII will log the number and type of engines on the loaded aircraft.'),
                    (28, 'Door 0 Logging', 'KACARS_LOG_DOOR_0', '1',
			'If ON then kACARSII will log changes of door 0. Typically the main aircraft door.'),
                    (29, 'Reverse Thrust', 'KACARS_REVERSE_THRUST', '1',
			'If ON then kACARSII will log when the reverse thryst is started and stopped.  Also the GS at these moments'),
                    (30, 'Auto Pilot', 'KACARS_LOG_AUTOPILOT', '1',
			'If ON then kACARSII will log when the auto pilot is activated or deactivated.  Also the altitude if the aircraft is in the air.'),
                    (31, 'Altimeter Setting', 'KACARS_LOG_ALTIMETER', '1',
            'If ON then kACARSII will log the altimeter setting on takeoff and landing.')
		";

        DB::query($sql);
        
        echo DB::$error;
    }    
    
    
    
    /***************************************************
     * Global Scoring Settings
     ***************************************************/
    
    public static function getScoring()
    {
        self::checkDB_Scoring();
        
        $sql = 'SELECT *
                    FROM ' . TABLE_PREFIX . 'kacars_scoring
                    ';
        $res = DB::get_results($sql);
        return $res;        
    }
    
    public static function updateScoring($id, $setting, $value, $delay, $repeat, $reset, $active)
    {
        self::checkDB_Settings();

        $id             = intval($id);
        $setting        = intval($setting);        
        $value          = intval($value);
        $delay          = intval($delay);
        $repeat         = intval($repeat);
        $reset          = intval($reset);
        $active         = intval($active);
        
        $sql = 'UPDATE `' . TABLE_PREFIX . 'kacars_scoring`
			SET 
                        `setting` = ' . $setting .',
                        `value` = ' . $value . ',
                        `delay` = ' . $delay . ',
                        `repeat` = ' . $repeat . ',
                        `reset` = ' . $reset . ',
                        `active` = ' . $active . '
			WHERE `id`=' . $id
        ;

        $res = DB::query($sql);

        if (DB::errno() != 0)
            return false;
        return true;
        
    }
    
    public static function checkDB_Scoring()
    {
        $table_name = TABLE_PREFIX . "kacars_scoring";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
                (
                    `id` int(11) unsigned NOT NULL,
                    `name` varchar(255) DEFAULT NULL,
                    `description` text,
                    `setting` int(11) NOT NULL DEFAULT '0',
                    `value` int(11) NOT NULL DEFAULT '0',
                    `delay` int(11) NOT NULL DEFAULT '0',
                    `repeat` tinyint(1) NOT NULL DEFAULT '0',
                    `reset` int(11) NOT NULL DEFAULT '0',
                    `active` tinyint(1) NOT NULL DEFAULT '1'
                )
                ENGINE=InnoDB;				
                ";

        DB::query($sql);     

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
        
        self::installRows_Scoring();
    }   

    public static function installRows_Scoring($auto = true)
    {
        $table_name = TABLE_PREFIX . "kacars_scoring";
        
        $sql = "SELECT * FROM `$table_name`";
        $res = DB::get_results($sql);

        if (count($res) > 1 && $auto)
            return;

        $sql = "
                INSERT IGNORE INTO `$table_name` (`id`, `name`, `description`, `setting`, `value`, `delay`, `repeat`, `reset`, `active`) 
                    VALUES
                        (1, 'S_SETUP', 'This Controls the setup of the scoring system.  If TRUE then the scoring system will be active.  
                            If FALSE then the scoring system will be disabled.  The Value is the starting score. The scoring system will 
                            deduct or add points to this score.', 
                            0, 100, 0, 0, 0, 1),
                        (2, 'S_ASCENT', 'Ascent point.  Setting determines the maximum VS in fpm.  If this setting is exceeded the
                            value is added/reduced from the current score. Seconds determines the number of seconds the setting must 
                            be exceeded before the penalty is assessed. (Setting should be a positive integer)', 
                            7000, -10, 2, 1, 5000, 1),
                        (3, 'S_DESCENT', 'Descent point.  Setting determines the maximum VS in fpm.  If this setting is exceeded the value
                            is added/reduced from the current score. Seconds determines the number of seconds the setting must be exceeded 
                            before the penalty is assessed. (Setting should be a negative integer)', 
                            -7000, -10, 2, 1, -5000, 1),
                        (4, 'S_OVERSPEED', 'Overspeed. (TRUE/FALSE) If active the application will monitor for an overspeed condition.',  
                            0, -10, 5, 1, 0, 1),
                        (5, 'S_STALL', 'Stall. (TRUE/FALSE) If active the application will monitor for a stall condition.',  
                            0, -10, 5, 1, 0, 1),
                        (6, 'S_CRASH', 'Crash. (TRUE/FALSE) If active the application will monitor for a crash condition. (Delay is ignored)',  
                            0, -100, 0, 0, 0, 1),
                        (7, 'S_SLEW', 'Slew. (TRUE/FALSE) If active the application will monitor for a slew condition.',  
                            0, -10, 5, 1, 0, 1),
                        (8, 'S_BANKANGLE', 'Bank Angle. Monitors the bank angle of the aircraft.  If bank angle exceeds this setting
                            the penalty is triggered. (Setting should be a positive integer)',  
                            50, -5, 5, 1, 25, 1),
                        (9, 'S_TIMEACCEL', 'Time Acceleration. Monitors for time acceleration.  If acceleration (Time Acceleration above setting)
                            is monitored the penalty is triggered. (Good Values = 1, 2, 4, 8, 16, 32, 64, 128)',  
                            1, -5, 5, 1, 1, 1),
                        (10, 'S_HARDLANDING', 'Hard Landing. Tiggered is landing rate (Vertical Speed at landing in FPM) is greater than the setting. 
                            (Setting should be a negative integer) (Delay is ignored) (Ignored if using aircraft profiles)',  
                            -1000, -20, 5, 0, 0, 1),
                        (11, 'S_MIDAIRREFUEL', 'Mid-Air Refuel. (TRUE/FALSE) Tiggered if a mid air refuel is monitored. (Setting, Delay, Repeat, Reset all 
                            are ignored with this parameter set.',  
                            0, -5, 0, 0, 0, 1),
                        (12, 'S_LITE_NAVIGATION', 'Navigation Lights OFF while in air. (TRUE/FALSE) Triggered if the aircraft is in the air and the navigation lights
                            are not on.',  
                            0, -5, 0, 0, 0, 1),
                        (13, 'S_LITE_STROBE', 'Strobe Lights OFF while in air. (TRUE/FALSE) Triggered if the aircraft is in the air and the strobe lights
                            are not on.',  
                            0, -5, 0, 0, 0, 1),
                        (14, 'S_LITE_BEACON', 'Beacon Lights OFF while in air. (TRUE/FALSE) Triggered if the aircraft is in the air and the beacon lights
                            are not on.',  
                            0, -5, 0, 0, 0, 1),
                        (15, 'S_LITE_LANDING_UP', 'Landing Lights ON above X. If the landing lights are ON above the setting value the penalty is triggered.',  
                            10500, -5, 5, 0, 10000, 1),
                        (16, 'S_LITE_LANDING_DOWN', 'Landing Lights OFF below X. If the landing lights are OFF below the setting value the penalty is triggered.',  
                            9500, -5, 5, 0, 10000, 1),
                        (17, 'S_SPEED_BELOW', 'If the aircraft IAS exceeds the setting below 10,000 ft AMSL the penalty is triggered.',  
                            260, -5, 3, 1, 220, 1),
                        (18, 'S_CRASH_DETECTION', 'Crash Detection Realism Setting. (TRUE/FALSE) Triggered if the crash detection is not activated. (Delay is ignored)',  
                            0, -10, 0, 0, 0, 1),
                        (19, 'S_CRASH_DETECTION_AIRCRAFT', 'Crash Detection With Aircraft Realism Setting. (TRUE/FALSE) Triggered if the crash detection with other aircraft 
                            is not activated. (Delay is ignored)',  
                            0, -10, 0, 0, 0, 1),
                        (20, 'S_UNLIMITED_FUEL', 'Unlimited Fuel Realism Setting. (TRUE/FALSE) Triggered if the unlimited fuel setting is activated. (Delay is ignored)',  
                            0, -20, 0, 0, 0, 1),
                        (21, 'S_TAXI_SPEED', 'Taxi Speed. Taxiing is determined on the status of the landing lights.  If the landing lights are OFF then it is assumed
                            that the aircraft is taxiing. (Setting should be a positive integer)',  
                            30, -5, 5, 1, 20, 1),
                        (22, 'S_LONG_FLIGHT', 'Long Flight. This can be used to give a bonus to a user if they fly a long flight.  The setting should be in minutes.  
                            This setting will be compared to the scheduled flight time and also the actual flight time.  If both of these are greater than this setting
                            the value will be awarded.  Value should be positive to provide a bonmus. (Setting should be a positive integer)',  
                            360, 20, 0, 0, 0, 1),                            
                        (23, 'S_ONTIME_SHORT', 'Ontime bonus for flights 0-2 hours.  The setting is the number of minutes for the ontime window.  The actual flight time
                            will be compared to the scheduled flight time. (Setting - window number of minutes).',  
                            5, 5, 0, 0, 0, 1),
                        (24, 'S_ONTIME_MEDIUM', 'Ontime bonus for flights 2-4 hours.  The setting is the number of minutes for the ontime window.  The actual flight time
                            will be compared to the scheduled flight time. (Setting - window number of minutes).',  
                            10, 5, 0, 0, 0, 1),
                        (25, 'S_ONTIME_LONG', 'Ontime bonus for flights 4-8 hours.  The setting is the number of minutes for the ontime window.  The actual flight time
                            will be compared to the scheduled flight time. (Setting - window number of minutes).',  
                            15, 5, 0, 0, 0, 1),
                        (26, 'S_ONTIME_EXTENDED', 'Ontime bonus for flights 8+ hours.  The setting is the number of minutes for the ontime window.  The actual flight time
                            will be compared to the scheduled flight time. (Setting - window number of minutes).',  
                            20, 5, 0, 0, 0, 1),
                        (27, 'S_RUNWAY_TAKEOFF', 'Determine if the user used the proper runway for taking off.  Proper runway is determined by the simulator wind condition. 
                            if the wind speed is under 10 knots this is ignored.  If the runway heading differes from the wind heading by 91 deg (+/-) then this penalty
                            will trigger. If the takeoff runway can not be determined this will be ignored.',  
                            0, -10, 0, 0, 0, 1),
                        (28, 'S_RUNWAY_LANDING', 'Determine if the user used the proper runway for landing.  Proper runway is determined by the simulator wind condition. 
                            if the wind speed is under 10 knots this is ignored.  If the runway heading differes from the wind heading by 91 deg (+/-) then this penalty
                            will trigger. If the takeoff runway can not be determined this will be ignored.',  
                            0, -10, 0, 0, 0, 1),
                        (29, 'S_PITCHANGLE', 'Pitch Angle. Monitors the pitch angle of the aircraft.  If pitch angle exceeds this setting
                            the penalty is triggered. (Setting should be a positive integer)',  
                            30, -5, 5, 1, 0, 1),
                        (30, 'S_ALTIMETER', 'Altimeter setting. Compares the aircraft altimeter setting to the actual. If these are different on takeoff or landing the scoring will be triggered.',  
                            0, -5, 0, 1, 0, 1);
               ";
        
        DB::query($sql);
    }
    
    public static function resetTable_Scoring()
    {
        $table_name = TABLE_PREFIX . "kacars_scoring";
        $sql = "DROP TABLE IF EXISTS `$table_name`";
        DB::query($sql);
        self::checkDB_Scoring();
    }
    
    
    
    /****************************************************
     * Aircraft Profile - Scoring Settings
     ****************************************************/
    
    public static function getScoring_AircraftAll($active = true)
    {
        self::checkDB_Scoring_Aircraft();
        
        $sql = 'SELECT * FROM ' . TABLE_PREFIX . 'kacars_scoring_aircraft';
        
        if($active)
            $sql .= ' WHERE `active` = 1';

        $sql .= ' ORDER BY `icao` DESC';
        
        $res = DB::get_results($sql);
        return $res;        
    }
    
    public static function getScoring_Aircraft($icao)
    {
        self::checkDB_Scoring_Aircraft();
        $icao = DB::escape($icao);

        $sql = 'SELECT *
                    FROM ' . TABLE_PREFIX . 'kacars_scoring_aircraft
                    WHERE `icao` = \'' . $icao . '\'
                    ';
        $res = DB::get_row($sql);        
        $obj = json_decode($res->json);
        
        if(!$obj)
            return false;
        
        $obj->icao = $icao;
        $obj->id = $res->id;
        return $obj;
    }
    
    public static function createScoring_Aircraft($fields)
    {        
        $cols[] = "`icao`";
        $col_values[] = "'{$fields['icao']}'";
        
        $cols[] = "`active`";
        $col_values[] = $fields['active'];

        $cols = implode(', ', $cols);
        $col_values = implode(', ', $col_values);
        $sql = 'INSERT INTO ' . TABLE_PREFIX . "kacars_scoring_aircraft ({$cols}) VALUES ({$col_values});";
        
        DB::query($sql);        
        $id = DB::$insert_id;
        self::editScoring_Aircraft($id, $fields);
        return $id;
    }
    
    public static function editScoring_Aircraft($id, $fields)
    {        
        $field = array();
        $field['id'] = $id;
        $fields['id'] = $id;
        $field['active'] = $fields['active'];
        $field['json'] = json_encode($fields);  
        $sql = "UPDATE `" . TABLE_PREFIX . "kacars_scoring_aircraft` SET ";
        $sql .= DB::build_update($field);
        $sql .= ' WHERE `id`=' . $id;        
        DB::query($sql);
    }
    
    public static function deleteScoring_Aircraft($id)
    {
        $id = intval($id);

        $sql = "DELETE FROM " . TABLE_PREFIX . "kacars_scoring_aircraft
                    WHERE `id`=$id";

        DB::query($sql);        
    }
    
    public static function checkDB_Scoring_Aircraft()
    {
        $table_name = TABLE_PREFIX . "kacars_scoring_aircraft";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
                (
                    `id` int(11) unsigned NOT NULL,
                    `icao` varchar(255) DEFAULT NULL,
                    `active` tinyint(1) NOT NULL DEFAULT '1',
                    `json`  longtext
                )
                ENGINE=InnoDB;				
                ";
        
        DB::query($sql); 

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`icao`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");       
    }
    
    public static function updateTable_Scoring_Aircraft()
    {
        
    }
    
    public static function resetTable_Scoring_Aircraft()
    {
        $table_name = TABLE_PREFIX . "kacars_scoring_aircraft";
        $sql = "DROP TABLE IF EXISTS `$table_name`";
        DB::query($sql);
        self::checkDB_Scoring_Aircraft();        
    }
    
    
    
    /**************************************************
     * Charter Control for individual users
     **************************************************/
    
    public static function getCharterUsers()
    {
        self::checkDB_CharterUser();

        $sql = 'SELECT a.*, p.*
                    FROM ' . TABLE_PREFIX . 'kacars_charter_users a
                    LEFT JOIN ' . TABLE_PREFIX . 'pilots p ON p.pilotid = a.pilotid';
                
        $res = DB::get_results($sql);        
        return $res;
    }
    
    public static function getCharterUser($pilotid)
    {
        self::checkDB_CharterUser();

        $sql = 'SELECT a.*, p.*
                    FROM ' . TABLE_PREFIX . 'kacars_charter_users a
                    LEFT JOIN ' . TABLE_PREFIX . 'pilots p ON p.pilotid = a.pilotid
                    WHERE a.pilotid = ' . $pilotid . '
                    LIMIT 1';
                
        $res = DB::get_row($sql);
        return $res;
    }
    
    public static function addCharterUser($pilotid)
    {
        self::checkDB_CharterUser();

        $pilotid = intval($pilotid);
        if ($pilotid == 0)
            return false;
        
        if(! self::getCharterUser($pilotid))
        {
            $sql = "INSERT INTO " . TABLE_PREFIX . "kacars_charter_users (
                        `pilotid`)
                        VALUES ( '$pilotid')";

            return DB::query($sql);
        }        
        return true;
    }
    
    public static function deleteCharterUser($pilotid)
    {
        $pilotid = intval($pilotid);

        $sql = "DELETE FROM " . TABLE_PREFIX . "kacars_charter_users
                    WHERE `pilotid`=$pilotid";

        DB::query($sql);        
    }
    
    public static function checkDB_CharterUser()
    {
        $table_name = TABLE_PREFIX . "kacars_charter_users";

        $sql = "CREATE TABLE IF NOT EXISTS `$table_name`
			(
				`id` int(11) NOT NULL,
				`pilotid` int(11) NOT NULL
			)
			ENGINE=MyISAM;				
			";

        DB::query($sql);

        DB::query("ALTER TABLE `$table_name` ADD PRIMARY KEY (`id`)");
        DB::query("ALTER TABLE `$table_name` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT");
    }    
    
}


class kACARSII_AircraftProfile extends CodonData
{
    public $flaps01_spd_max     = NULL;
    public $flaps02_spd_max     = NULL;
    public $flaps03_spd_max     = NULL;
    public $flaps04_spd_max     = NULL;
    public $flaps05_spd_max     = NULL;
    public $flaps06_spd_max     = NULL;
    public $flaps07_spd_max     = NULL;
    public $flaps08_spd_max     = NULL;
    public $flaps09_spd_max     = NULL;
    public $flaps10_spd_max     = NULL;
    
    public $flaps01_points      = NULL;
    public $flaps02_points      = NULL;
    public $flaps03_points      = NULL;
    public $flaps04_points      = NULL;
    public $flaps05_points      = NULL;
    public $flaps06_points      = NULL;
    public $flaps07_points      = NULL;
    public $flaps08_points      = NULL;
    public $flaps09_points      = NULL;
    public $flaps10_points      = NULL;   
    
    public $gear_spd_max        = NULL;
    public $gear_points         = NULL;
    
    public $fuel_land_min       = NULL;
    public $fuel_land_points    = NULL;
    
    public $landing_low         = NULL;
    public $landing_mid         = NULL;
    public $landing_high        = NULL;
    
    public $landing_soft_points = NULL;
    public $landing_low_points  = NULL;
    public $landing_mid_points  = NULL;
    public $landing_high_points = NULL;
    
    public $max_weight_takeoff          = NULL;
    public $max_weight_takeoff_points   = NULL;
    
    public $max_weight_landing          = NULL;
    public $max_weight_landing_points   = NULL;    
    
    public $active              = NULL;            
    
}
