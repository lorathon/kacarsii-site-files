<?php

/*
 * kACARSII Route Map (Frontend)
 * By: Jeffrey Kobus
 * www.fs-products.net 
 * 02/21/2018
 * v1.0.8.0
 */
?>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css" 
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" 
    crossorigin=""
/>

<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" 
    integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" 
    crossorigin="">
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<h3>Route Map</h3>
<div class="mapcenter" align="center">
    <div id="mapid" style="width: <?php echo Config::Get('MAP_WIDTH'); ?>; height: <?php echo Config::Get('MAP_HEIGHT') ?>"></div>
</div>

<?php

// Set map data based on pirep data
if (isset($pirep))
    $mapdata = $pirep;

// Set map data based on schedule data
if (isset($schedule))
    $mapdata = $schedule;
?>


<?php

// Set airport icons
$icondep = fileurl('/lib/images/icon_dep.png');
$iconarr = fileurl('/lib/images/icon_arr.png');
$iconvor = fileurl('/lib/images/icon_vor.png');
$iconfix = fileurl('/lib/images/icon_fix.png');
?>

<script type="text/javascript">
    
    var routePoints = [];
    var trackPoints = [];
    var bounds;
    
    var kacars_map = L.map('mapid', {
    	center: [0, 0],
    	zoom: 13,
    	minZoom: 0,
    	maxZoom: 218
    });
			
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=<?php echo kACARSIIData::getSetting('KACARS_MAPBOXTOKEN'); ?>', {
	maxZoom: 18,
	attribution: 'Map script &copy; <a href="http://www.fs-products.net/">FS-Products</a>',
	id: 'mapbox.satellite'
	}).addTo(kacars_map);
        
    // Departure Airport Icon
    var depIcon = L.icon({
        iconUrl: '<?php echo $icondep; ?>',
        iconSize: [20, 20],
        iconAnchor: [10, 10]
        });

    // Arrival Airport Icon
    var arrIcon = L.icon({
        iconUrl: '<?php echo $iconarr; ?>',
        iconSize: [20, 20],
        iconAnchor: [10, 10]
        });		

    // NAVAID Vor Icon
    var vorIcon = L.icon({
        iconUrl: '<?php echo $iconvor; ?>',
        iconSize: [19, 20],
        iconAnchor: [10, 10]
        });

    // NAVAID Fix Icon
    var fixIcon = L.icon({
        iconUrl: '<?php echo $iconfix; ?>',
        iconSize: [12, 15],
        iconAnchor: [6, 15]
        });		
    
    // Add Airport Markers
    var depCoordinates = [<?php echo $mapdata->deplat ?>, <?php echo $mapdata->deplng; ?>];

    var arrCoordinates = [<?php echo $mapdata->arrlat ?>, <?php echo $mapdata->arrlng; ?>];

    L.marker(depCoordinates, {icon: depIcon})
            .addTo(kacars_map)
            .bindPopup("<?php echo $mapdata->depname; ?>");

    L.marker(arrCoordinates, {icon: arrIcon})
            .addTo(kacars_map)
            .bindPopup("<?php echo $mapdata->arrname; ?>");   
    
    routePoints.push(depCoordinates);
    
<?php
/* Populate the route */
if (is_array($mapdata->route_details)) {    
    
    foreach ($mapdata->route_details as $route) {
        if ($route->type == NAV_VOR)
            $icon = 'vorIcon';
        else
            $icon = 'fixIcon';
        
        $lat = str_replace(",", ".", $route -> lat);
        $lng = str_replace(",", ".", $route -> lng);
        
        $type = "TRACK";
        if($route->type == 2) $type = "NDB";
        if($route->type == 3) $type = "VOR";
        if($route->type == 4) $type = "DME";
        if($route->type == 5) $type = "FIX";
        if($route->type == 6) $type = "TRACK";
        
        // Build Route details
        $details = "Frequency: <b>{$route->freq}</b><br/> "
            . "Name: <b>{$route->name}</b><br/> "
            . "Title: <b>{$route->title}</b><br/> "
            . "Type: <b>{$type}</b><br/> "
            . "Latitude: <b>".(round($lat, 4))."</b><br/> "
            . "Longitude: <b>".(round($lng, 4))."</b><br/> ";
        ?>
        
        var routeCoordinates = [<?php echo $lat ?>, <?php echo $lng ?>];
        
        L.marker(routeCoordinates, {
            icon: <?php echo $icon; ?>,
            title: '<?php echo $route->name; ?>'
            })
            .addTo(kacars_map)
            .bindPopup("<?php echo $details; ?>");
       
         routePoints.push(routeCoordinates);
         
        <?php
    }
}

/* Populate the track */
$res = @unserialize($mapdata->rawdata);
if ($res !== false) {
    $mapdata -> rawdata = $res;
    unset($res);
}

if (is_array($mapdata->rawdata['points'])) {    
    
    foreach ($mapdata->rawdata['points'] as $point) {
        
        $detail = "";
        if ($point['alt'] < 10 && $point['warning'] == 0) {
            $icon = fileurl('/lib/images/onground.png');
        }
        if ($point['warning'] == 1) {
            $icon = fileurl('/lib/images/warning.png');
        }
        if ($point['alt'] > 10 && $point['warning'] == 0) {
            $icon = fileurl('lib/images/inair/' . $point['head'] . '.png');
        }

        if ($point['warning'] == 1) {
            $detail = ' | Warning: ' . $point['warningdetail'];
        }

        $latP = str_replace(",", ".", $point['lat']);
        $lngP = str_replace(",", ".", $point['lng']);

        if ($latP == '0')
            continue;
        
        // Build Track Point details
        $details = "Altitude: <b>{$point['alt']}</b><br/> "
            . "GS: <b>{$point['gs']}</b><br/> "
            . "Heading: <b>{$point['head']}</b><br/> "
            . "Phase: <b>{$point['phase']}</b><br/> "
            . "Latitude: <b>".(round($latP, 4))."</b><br/> "
            . "Longitude: <b>".(round($lngP, 4))."</b><br/> ";
        ?>
            
        // Track Point Icon
        var trackIcon = L.icon({
            iconUrl: '<?php echo $icon; ?>',
            iconSize: [41, 41],
            iconAnchor: [20, 20]
            });	
        
        var trackCoordinates = [<?php echo $latP.', '.$lngP; ?>];        
                
        L.marker(trackCoordinates, {icon: trackIcon})
            .addTo(kacars_map)
            .bindPopup("<?php echo $details; ?>");
    
        trackPoints.push(trackCoordinates);         
        <?php
    }
    ?>
    bounds = new L.LatLngBounds(trackPoints);
    
    // Construct track line
    L.polyline(trackPoints, {color: 'green'})
	.addTo(kacars_map);

    <?php
} else {
    ?>
    bounds = new L.LatLngBounds(routePoints);
    <?php
}
?>       
    // Add arrivl airport to route line
    routePoints.push(arrCoordinates);
    
    // Construct route line
    L.polyline(routePoints, {color: 'red'})
	.addTo(kacars_map);    
    
    // Fit map to bounds    
    kacars_map.fitBounds(bounds);
    
</script>