<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<h3>kACARSII Log Settings</h3>

<p>Select available logging options from this page. Don't forget to save!</p>

<div  style="float:right">  
    <button class="{button:{icons:{primary:'ui-icon-info'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/updateLog');?>';"> 
            Update Logging DataBase
    </button>  
    <button class="{button:{icons:{primary:'ui-icon-alert'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/resetLogToDefault');?>';"> 
        Reset Logging Database (all settings will be lost)
    </button>
</div>


<form id="form" method="post" action="<?php echo SITE_URL ?>/admin/action.php/kacarsadmin/logsettings">

    <table id="tabledlist" class="tablesorter">
        <thead>
            <tr>
                <th>Setting Name</th>
                <th width="10%">Status</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!$settings)
            {
                echo '<p>No log settings have been added</p>';
            }
            else
            {
                foreach ($settings as $setting)
                {
                    echo '<tr>
			<td width="15%" nowrap>
			<strong>' . $setting->title . '</strong></td>
			<td>';
                    
                    $checked = ($setting->value == 1) ? 'CHECKED' : '';
                    $unchecked = ($setting->value == 0) ? 'CHECKED' : '';
                    echo '<input type="radio" name="' . $setting->name . '" value="1" ' . $checked . '/>ON<br/>';
                    echo '<input type="radio" name="' . $setting->name . '" value="0" ' . $unchecked . '/>OFF';    
                    echo '</td><td>' . $setting->description . '</td></tr>';
                }
            }
            ?>
            <tr>	
                <td>
                    <input type="hidden" name="action" value="savesettings">
                    <input type="submit" name="submit" value="Save Settings" />
                </td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
</form>