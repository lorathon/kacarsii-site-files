<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<h3>kACARSII Aircraft Profiles</h3>

<p>If the scoring system option has been purchased you can modify how the option works using these parameters.</p>

<button class="{button:{icons:{primary:'ui-icon-plusthick'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/createScoringAircraft');?>';">
    Add Aircraft Profile
</button>
<div  style="float:right">
    <button class="{button:{icons:{primary:'ui-icon-info'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/update_DataBase_AircraftProfile');?>';"> 
        Update Aircraft Profile DataBase
    </button>
    <button class="{button:{icons:{primary:'ui-icon-alert'}}}" onclick="window.location='<?php echo adminurl('/kacarsadmin/reset_DataBase_AircraftProfile');?>';"> 
        Reset Aircraft Profile Database (all profiles will be lost)
    </button>
</div>

<table id="tabledlist" class="tablesorter">
    <thead>
        <tr>
            <th>ICAO</th>
            <th>Active</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
    <?php
    if(count($records) > 0)
    {    
        foreach($records as $record)
        {
        ?>
            <tr id="row<?php echo $record->id;?>">
                <td><?php echo $record->icao; ?></td>
                <td><strong><?php echo ($record->active == '1' ? 'TRUE' : '<font color="red">FALSE</font>'); ?></strong></td>
                <td align="center" width="1%" nowrap>
                    <button class="{button:{icons:{primary:'ui-icon-wrench'}}}" 
                        onclick="window.location='<?php echo adminurl('/kacarsadmin/editScoringAircraft?icao=' . $record->icao);?>';">
                        Edit Profile
                    </button>
                    <button href="<?php echo SITE_URL?>/admin/action.php/kacarsadmin/deleteScoringAircraft/<?php echo $record->id; ?>" action="delete" 
                        id="<?php echo $record->id;?>" class="deleteitem {button:{icons:{primary:'ui-icon-trash'}}}">
                        Delete
                    </button>
                </td>
            </tr>
        <?php
        }
    }
    else
    {
    ?>
            <tr>
                <td>No records found</td>
            </tr>
            
    <?php
    }
    ?>
    </tbody>
</table>

