<?php

/**
 * This is an example of hooking into the kACARSII
 * events.  This module should not be used except for
 * an example to build a custom module.  This module
 * will be overwritten by kACARSII file updates
 * 
 * visit wiki.fs-products.net for more information
 * 
 * FS-Products
 * www.fs-products.net
 * Jeffrey Kobus
 * 02/21/2018
 * v1.0.8.0
 */

class kACARSII_Test extends CodonModule 
{       
    public function __construct() 
    {        
        CodonEvent::addListener('kACARSII_Test', array('kACARSII_login', 'kACARSII_liveupdate', 'kACARSII_pirep_filed'));        
    }   
    
    public function EventListener($eventinfo) 
    {     
        $xml = $eventinfo[2];
        
        if($eventinfo[0] == 'kACARSII_login') 
        {
            /*
             * kACARSII client has attempted to login
             * xml will have all data for 
             * the login
             * 
             * Do stuff below and return out of here
             */          
                        
            return;
        }
        
        if($eventinfo[0] == 'kACARSII_liveupdate')
        {
            /*
             * kACARSII has updated the acars table
             * xml will have all data for the update
             * 
             * Do stuff below and return out of here
             */      
         
            return;
        }
        
        if($eventinfo[0] == 'kACARSII_pirep_filed') 
        {
            /*
             * kACARSII has filed a pirep
             * xml will have all data for 
             * the pirep
             * 
             * Do stuff below and return out of here
             */      
            
            return;
        }        
        
        
    }    
}

