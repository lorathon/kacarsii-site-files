<h3>Assign Pilot to AFK</h3>

<form id="addafkuser" method="POST" action="<?php echo adminurl('/kacarsadmin/afkusers'); ?>">
    <select name="pilotid">
        <?php
        foreach ($pilots as $pilot)
        {
            $pilotcode = PilotData::getPilotCode($pilot->code, $pilot->pilotid);
            echo '<option value="' . $pilot->pilotid . '"> ' . $pilotcode . ' ' . $pilot->firstname . ' ' . $pilot->lastname . '</option>';
        }
        ?>
    </select>
    <input type="hidden" name="action" value="addafkuser" />
    <input type="submit" name="submit" value="Asign AFK User" />
</form>


<div id="afkuserlist">
    <h3>Pilots Required to use AFK</h3>
    <?php
    if (!$afkpilots)
    {
        echo 'No pilots are required to use AFK!</div>';
        return;
    }
    ?>
    <table id="tabledlist" class="tablesorter">
        <thead>
            <tr>
                <th>Pilot</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($afkpilots as $afk)
            {
                $pilotcode = PilotData::getPilotCode($afk->code, $afk->pilotid);
                ?>
                <tr>
                    <td><?php echo $pilotcode . ' ' . $afk->firstname . ' ' . $afk->lastname; ?></td>
                    <td>
                        <button class="{button:{icons:{primary:'ui-icon-trash'}}}" 
                                onclick="window.location = '<?php echo adminurl('/kacarsadmin/deleteafkuser/' . $afk->pilotid); ?>';">Delete</button>
                    </td>
                </tr>

                <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script>
    $('.afkdeleteajaxcall').live('click', function () {
        $("#afkuserlist").load($(this).attr("href"), {action: $(this).attr("action"), id: $(this).attr("id"), pilotid: $(this).attr("pilotid")},
                function (d) {
                    $('a.button, button, input[type=submit]').button();
                })
    });
</script>