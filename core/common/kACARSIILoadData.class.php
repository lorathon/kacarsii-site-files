<?php

/*
 * kACARSII Load Data
 * By: Jeffrey Kobus
 * www.fs-products.net
 * 02/21/2018
 * v1.0.7.0
 */

class kACARSIILoadData extends CodonData
{

    /************************************************
     * Load Control
     * These are basic methods that can be changed to include 
     * customized load generation.  These files should not
     * change in the future unless there is a basic error in
     * the script.  It is recommended that the methods
     * below are used to create the load generatrion script based
     * on the flightnumber data or the bidid number.
     ************************************************/

    public static function getPaxLoad($flightnum = null, $bidid = null)
    {
        return 0;
    }

    public static function getCargoLoad($flightnum = null, $bidid = null)
    {
        return 0;
    }
}